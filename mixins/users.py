#!/usr/bin/env python
# Skaro is the open standard for instant messaging
# Copyright © 2017 Exarh Team
#
# This file is part of skaro-server.
#
# Skaro-server is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging
import copy
from random import randint

# Tornado server
import tornado.escape
import tornado.util
import tornado.ioloop
import tornado.web
import tornado.gen
import tornado.websocket
from tornado.options import options

# for encryption
import base64
from Crypto.Hash import SHA256
from Crypto.Cipher import PKCS1_v1_5
from Crypto.PublicKey import RSA

# Local modules and packages
import protocols
from modules import config, database, helpers, validate
from modules.validate import DictSchema, IntValue, BoolValue, StringValue, ListValue

logger = logging.getLogger(__name__)


class UsersManagement:
    """
    @DynamicAttrs - for understanding attributes in base class and other mixins (for PyCharm)
    """

    session = None
    device_id = None
    uid = None

    def is_auth(self):
        return self.session is not None and hasattr(self, "session_id")

    # Событие вызываемое отбработчиком соединения другого клиента при изменении данных текущего
    # Часто оно задействуется для синхронизации данных на между активными устройствами одного пользователя
    @tornado.web.asynchronous
    def sysevent_changed_user_data(self, data):
        # todo: необходимо тщательнее продумать API
        """
        try:

            # Пришло уведомление об изменении логина из списка контактов
            if "login" in data["update"].keys():
                # Пользователь ИЗ КОНТАКТОВ изменил свой логин
                if data["login"] != self.session.data["login"]:
                    # Меняем данные если юзер с таким логином действительно есть в контактах
                    if data["login"] in self.session.data["contacts"]:
                        # Кроме того, мы уже могли поменять логин.
                        if data["update"]["login"] not in self.session.data["contacts"]:
                            # и, наконец, меняем логин контакта
                            self.session.data["contacts"][data["update"]["login"]]\
                                = self.session.data["contacts"][data["login"]]

                            del self.session.data["contacts"][data["login"]]


        except Exception:
            logger.error("Error handling data: needed key is not exists", exc_info=True)
            return
        """

        # Отправка клиентскому приложению
        self.write_client(data)

    # Отдельное событие вызываемое самим сервером при изменении списка контактов пользователя.
    # todo: Например, если пользователя добавили в конференцию, то конференция автоматически стала его контактом.
    # Например, если при одновременном входе с нескольких устройств вы удалили/добавили контакт,
    #  то необходимо известить об этом другие устройства.
    #  В таком случае, этот метод вызывается в обработчиках соединений всех устройств пользователя.
    def sysevent_changed_contacts(self, data):
        self.write_client(data)

    # Отправляет информацию группе активных устройств пользователей(по умолчанию это все, с пользователем в контактах)
    #  или текущего пользователя или участникам конференции
    @tornado.web.asynchronous
    @tornado.gen.engine
    def send_updates(self, data, users_for_update=None, for_conference=False, send_self_devices=False):
        if not type(data) is dict or "type" not in data.keys():
            logger.error("Error handling data: invalid format")
            return

        logger.info("sending message to all contacts")

        if users_for_update is None:
            # Загружаем список тех, кого уведомлять
            try:
                if for_conference:
                    cursor = yield database.getAtWhomInFavorites(for_conference, is_conference=True)
                else:
                    cursor = yield database.getAtWhomInFavorites(self.session.data["login"])
            except (database.Warning, database.Error):
                logger.error("Error getting users to send notifications", exc_info=True)
                return
            else:
                users_for_update = cursor.fetchall()

        # Удаление повторений
        users_for_update = set(users_for_update)

        # Если нужно, то добавляем свой логин.
        # Тогда сообщение отправится на все устройства пользователя, кроме текущего устройства.
        if send_self_devices:
            users_for_update.add(self.session.data["login"])

        # Уведомляем
        for us_login in users_for_update:
            us_login = helpers.recover_full_login(us_login)
            us_data = self.application.AuthorizedPool.get(
                helpers.parse_skaro_login(us_login)[1]
            )

            if us_data is None:
                #todo: если юзер не в онлайне ему тоже положено знать о некоторых событиях.
                #todo: Например, в сообщении на почту. Но это должно быть настраиваемо пользователем.
                continue

            for waiter in us_data.connections.values():
                if waiter.uid == self.uid: continue

                logger.info("Пользователю: %s (%s)", waiter.session.data['login'], waiter.device_id)

                if hasattr(waiter, "sysevent_"+data["type"]):
                    # С предварительной обработкой
                    getattr(waiter, "sysevent_"+data["type"])(data)
                else:
                    logger.error("Пользователю %s (%s) не удалось отправить сообщение.\
                                  Не найден обработчик сообщения этого типа",
                                 waiter.session.data['login'], waiter.device_id)

    # Возвращает данные о юзере
    # вызов только через user_data = yield self.get_user_data(login)
    @tornado.gen.coroutine
    def get_user_data(self, full_login):
        user = False
        (protocol, login_in_prl) = helpers.parse_login(full_login)
        if protocol == "skaro":
            (host, login) = helpers.parse_skaro_login(login_in_prl)

            # TODO: Если контакт - конференция
            if login[0] == "~":
                return False  # Аналогично raise tornado.gen.Return(False)

            if host == options.host:
                # Ищем юзера в онлайне
                user = {"status": "offline"}
                user_data = self.application.AuthorizedPool.get(
                    helpers.parse_skaro_login(full_login)[1]
                )
                if user_data is not None:
                    user = helpers.filtering_user_data(user_data.data.copy())

                if user["status"] == "offline":
                    try:
                        cursor = yield database.getUserByLogin(login[1:])
                    except (database.Warning, database.Error):
                        logger.error("Error in get user data", exc_info=True)
                        user = False
                    else:
                        user = cursor.fetchone()
                        if user:
                            user = helpers.filtering_user_data(dict(user))
                            user['login'] = helpers.recover_full_login(user['login'])
                            user["status"] = "offline"
                        else:
                            user = False
            else:
                # Обращение к другому skaro серверу...
                pass
        else:
            user = self.session.accounts[protocol].get_user_data(login_in_prl)
            if user:
                user = helpers.filtering_user_data(user)

        return user  # Аналогично raise tornado.gen.Return(user)

    # Получает полную информацию о контактах пользователя
    # вызов только через yield self.get_self_contacts()
    @tornado.gen.coroutine
    def get_self_contacts(self):
        contacts = []
        # Прогружаем данные контактов (тут только skaro протокол)
        for login, settings in self.session.data['contacts'].items():
            (protocol, login_in_prl) = helpers.parse_login(login)

            if protocol != "skaro":
                continue
            if settings.get("type") != "conference":
                user = yield self.get_user_data(login)
                if user:
                    contacts.append(user)
                else:
                    # Информируем пользователя
                    self.write_client({
                        "type": "error",
                        "status": "500 Internal Server Error",
                        "info": "Ошибка при попытке получить информацию о контактах пользователя"
                    })
            else:
                conf = yield self.get_conf_data(login)
                if conf:
                    contacts.append(conf)
                else:
                    # Информируем пользователя
                    self.write_client({
                        "type": "error",
                        "status": "500 Internal Server Error",
                        "info": "Ошибка при попытке получить информацию о контактах пользователя"
                    })

        # Добавляем контактов из доп. протоколов.
        for protocol, cls in self.session.accounts.items():
            if protocol in protocols.__all__:
                # Предварительно очищаем от контактов этого протокола старый список контактов
                contacts = [cn for cn in contacts if helpers.parse_login(cn["login"])[0] != protocol]
                # а уже потом добавляем
                try:
                    contacts += cls.get_contacts()
                except Exception:
                    # Отправляем клиенту, что произошла ошибка
                    self.write_client({
                        "type": "error",
                        "status": "500 Internal Server Error",
                        "info": "Ошибка при попытке \"%s\" получить информацию о контактах пользователя" % (protocol,),
                    })

        return contacts

    # Дополнительно заполняет данными обьект соединения с клиентом
    # вызов только через yield self.init_self_user()
    # прогружает список контактов(без полной о них информации), авторизуется в прикрепленных аккаунтах
    @tornado.gen.coroutine
    def init_self_user(self):

        # Получаем логины контактов, с которыми есть открытые диалоги
        try:
            cursor = yield database.getChats(self.session.data['login'])
        except (database.Warning, database.Error):
            contacts = self.session.data['contacts']
            logger.error("Error get contacts", exc_info=True)
            # Информируем пользователя
            self.write_client({
                "type": "error",
                "status": "500 Internal Server Error",
                "info": "Ошибка при попытке получить контакты пользователя",
            })
        else:
            # Добавляем их в контакт-лист, если их еще там нет
            contacts = {login: {} for login in cursor.fetchall()}
            contacts.update(self.session.data['contacts'])


        # Авторизуемся в прикрепленных протоколах и сохраняем интерфейс работы с ними.
        # Добавляем контактов из доп. протоколов.
        for login, settings in self.session.data['accounts'].items():
            (protocol, login_in_prl) = helpers.parse_login(login)
            if protocol in protocols.__all__:
                try:
                    mod = tornado.util.import_object('protocols.' + protocol)
                    cls = mod.common(self, settings)
                    if cls:
                        # Сохраняем интерфейс
                        self.session.accounts[protocol] = cls

                        # Предварительно очищаем от контактов этого протокола
                        contacts = {login: settings
                                        for login, settings in contacts.items()
                                            if helpers.parse_login(login)[0] != protocol}
                        # а уже потом добавляем
                        contacts.update(cls.get_contacts_settings())
                    else:
                        raise ImportError('Unable to create an instance of the handler in the module "%s"'
                                          % (protocol,))
                except Exception:
                    logger.error('Module %s not imported.' % (protocol), exc_info=True)
                    # Отправляем клиенту, что произошла ошибка
                    self.write_client({
                        "type": "error",
                        "status": "500 Internal Server Error",
                        "info": "Ошибка авторизации в %s" % (protocol),
                    })

        # Обновление списка контактов, если нужно
        if contacts != self.session.data['contacts']:
            self.session.data['contacts'] = contacts
            try:
                yield database.setUser({"contacts": self.session.data['contacts'], "login": self.session.data['login']})
            except (database.Warning, database.Error):
                logger.error("Error update contacts", exc_info=True)
                # Информируем пользователя
                self.write_client({
                    "type": "error",
                    "status": "500 Internal Server Error",
                    "info": "Ошибка при попытке обновить список контактов пользователя",
                })

    # Выход пользователя
    def event_logout(self, data: DictSchema(
            type = StringValue(required=True),
            not_close_connection = BoolValue(),
            chain_uuid = StringValue()
        )):
        if "not_close_connection" not in data.keys():
            data["not_close_connection"] = False

        logger.info("Пользователь вышел: " + self.session.data['login'] + "(" + str(self.device_id) + ")")

        # Удаляем из онлайна
        del self.session.connections[self.device_id]
        if not len(self.session.connections):
            self.session.data["status"] = "offline"
            self.send_updates({
                "type": "changed_user_data",
                "login": self.session.data['login'],
                "update": {
                    "status":"offline"
                }
            })
            del self.application.AuthorizedPool[helpers.parse_skaro_login(self.session.data['login'])[1]]

        # Добпавляем сесссию потерянного соединения в хранилище быстрого восстановления сессий
        self.application.add_session_in_store(
            session_id=self.session_id,
            login=self.session.data['login']
        )

        # заканчиваем операции для не авторизованных
        if data["not_close_connection"] is False:
            del self

    @tornado.web.asynchronous
    @tornado.gen.engine
    @validate.arguments
    def event_register(self, data: DictSchema(
            type = StringValue(required=True),
            chain_uuid = StringValue(required=True),
            invite = StringValue(max=255),
            login = StringValue(
                max=255,
                pattern=validate.P_LOGIN,
                prepare_callback=lambda x: helpers.parse_login(x, True)[2][1:],
                required=True
            ),
            email = StringValue(required=True, max=255),
            public_key = StringValue(required=True, max=458)
        )):

        if config.read('register_type') == 'invite':
            if not helpers.is_exist_keys(data, 'invite'):
                self.write_client({
                    "chain_uuid": data["chain_uuid"],
                    "type": "request",
                    "status": "400 Bad Request",
                    "info": "Для регистрации требуется приглашение",
                    "struct_type": "register"
                })
                return
            else:
                try:
                    cursor = yield database.getSomeInvite(data['invite'])
                except (database.Warning, database.Error):
                    logger.error("Error get invites", exc_info=True)
                    # Информируем пользователя
                    self.write_client({
                        "chain_uuid": data["chain_uuid"],
                        "type": "request",
                        "status": "500 Internal Server Error",
                        "info": "Ошибка при попытке проверки существования приглашения",
                        "struct_type": "register"
                    })
                    return
                else:
                    invite = cursor.fetchone()
                    if invite is None:
                        self.write_client({
                            "chain_uuid": data["chain_uuid"],
                            "type": "request",
                            "status": "400 Bad Request",
                            "info": "Приглашение не найдено",
                            "struct_type": "register"
                        })
                        return
                    elif not invite[2]:
                        self.write_client({
                            "chain_uuid": data["chain_uuid"],
                            "type": "request",
                            "status": "400 Bad Request",
                            "info": "Приглашение не действительно",
                            "struct_type": "register"
                        })
                        return

        # Элементарная проверка адреса почты, для предотвращения лишних действий
        if not validate.is_valid(data['email'], validate.P_MAIL):
            self.write_client({
                "chain_uuid": data["chain_uuid"],
                "type": "request",
                "status": "400 Bad Request",
                "info": "Недопустимый адрес почты",
                "struct_type": "register"
            })
            return

        # Элементарная проверка логина, для предотвращения лишних действий
        if not validate.is_valid(data['login'], validate.P_LOGIN):
            self.write_client({
                "chain_uuid": data["chain_uuid"],
                "type": "request",
                "status": "400 Bad Request",
                "info": "Недопустимый логин",
                "struct_type": "register"
            })
            return

        try:
            cursor = yield database.getSomeEMails(data['email'])
        except (database.Warning, database.Error):
            logger.error("Error get count", exc_info=True)
            # Информируем пользователя
            self.write_client({
                "chain_uuid": data["chain_uuid"],
                "type": "request",
                "status": "500 Internal Server Error",
                "info": "Ошибка при попытке проверки занятости адреса почты",
                "struct_type": "register"
            })
            return
        else:
            if cursor.fetchone()[0] > 0:
                self.write_client({
                    "chain_uuid": data["chain_uuid"],
                    "type": "request",
                    "status": "400 Bad Request",
                    "info": "Этот адрес почты уже занят",
                    "struct_type": "register"
                })
                return

        try:
            cursor = yield database.getSomeLogin(data['login'])
        except (database.Warning, database.Error):
            logger.error("Error get count", exc_info=True)
            # Информируем пользователя
            self.write_client({
                "chain_uuid": data["chain_uuid"],
                "type": "request",
                "status": "500 Internal Server Error",
                "info": "Ошибка при попытке проверки занятости логина, попробуйте еще раз",
                "struct_type": "register"
            })
            return
        else:
            if cursor.fetchone()[0] > 0:
                self.write_client({
                    "chain_uuid": data["chain_uuid"],
                    "type": "request",
                    "status": "400 Bad Request",
                    "info": "Этот логин уже занят",
                    "struct_type": "register"
                })
                return

        try:
            yield database.addUser(data)
        except (database.Warning, database.Error):
            logger.error("Error adding user", exc_info=True)
            self.write_client({
                "chain_uuid": data["chain_uuid"],
                "type": "request",
                "status": "400 Bad Request",
                "info": "Не удалось зарегистрироваться",
                "struct_type": "register"
            })
            return

        if config.read('register_type') == 'invite':
            try:
                yield database.markInvite(data['invite'])
            except (database.Warning, database.Error):
                logger.error("Error marking invite", exc_info=True)
                self.write_client({
                    "chain_uuid": data["chain_uuid"],
                    "type": "request",
                    "status": "400 Bad Request",
                    "info": "Не удалось зарегистрироваться",
                    "struct_type": "register"
                })
                return

        # Операция прошла успешно
        self.write_client({
            "chain_uuid": data["chain_uuid"],
            "type": "request",
            "status": "200 OK",
            "login": data['login'],
            "struct_type": "register"
        })

    @tornado.web.asynchronous
    @validate.arguments
    def event_register_type(self, data: DictSchema(
            type = StringValue(required=True),
            chain_uuid = StringValue(required=True),
        )):

        register_type = config.read('register_type')
        if register_type not in ['open', 'invite', 'close']:
            register_type = 'open'

        self.write_client({
            "chain_uuid": data["chain_uuid"],
            "type": "request",
            "status": "200 OK",
            "register_type": register_type,
            "struct_type": "register_type"
        })

    @tornado.web.asynchronous
    @tornado.gen.engine
    @validate.arguments
    def event_authorize(self, data: DictSchema(
            type = StringValue(required=True),
            chain_uuid = StringValue(required=True),
            login = StringValue(
                required=True,
                max=255,
                pattern=validate.P_LOGIN,
                prepare_callback=lambda x: helpers.parse_login(x, True)[2][1:]
            ),
            device_id=StringValue(max=64, pattern=validate.P_DEVICE_ID)
        )):

        # Элементарная проверка, для предотвращения лишних действий
        if validate.is_valid(data['login'], validate.P_MAIL):
            is_mail = 1
        elif validate.is_valid(data['login'], validate.P_LOGIN):
            is_mail = 0
        else:
            if helpers.is_request(data):
                self.write_client({
                    "chain_uuid": data["chain_uuid"],
                    "type": "request",
                    "status": "400 Bad Request",
                    "info": "Недопустимый логин",
                    "struct_type": "authorize_rnd"
                })
            return

        # Получение данных юзера по логину или email.
        # По идее, это не всегда нужно делать с обращением к БД.
        # Т.к. если пользователь входит в свой аккаунт одновременно с другим устройством,
        # эти данные уже получены и хранятся в оперативной памяти сервера.
        # Так что это пусть не идельное, но зато локаничное решение.
        # На общую производительность оно практически не влияет.
        try:
            if is_mail:
                cursor = yield database.getUserByEMail(data['login'])
            else:
                cursor = yield database.getUserByLogin(data['login'])
        except (database.Warning, database.Error):
            logger.error("Error get user data", exc_info=True)
            user = None
        else:
            user = cursor.fetchone()

        encrypted_rnd = ""
        rnd_hash = ""
        if user:
            rnd = str(randint(100000, 999999)).encode('utf-8')
            rnd_hash = SHA256.new(rnd).hexdigest()

            public_key = user['public_key']

            encrypter = PKCS1_v1_5.new(RSA.importKey(public_key))
            base64_enc = encrypter.encrypt(rnd)

            base64_enc = base64.b64encode(base64_enc)

            encrypted_rnd = base64_enc.decode("utf-8")

            status = '200 OK'
            info = ''
        else:
            status = '400 Bad Request'
            info = "Ошибка при попытке поиска пользователя с таким именем."

        if helpers.is_request(data):
            if status[0:3] == '200':

                try:
                    res = yield self.send_request({
                        "chain_uuid": data["chain_uuid"],
                        "type": "request",
                        "status": "200 OK",
                        "encrypted_rnd": encrypted_rnd,
                        "struct_type": "authorize_rnd"
                    },
                        DictSchema(
                            rnd_hash=StringValue(
                                required=True,
                                max=64
                            )
                        )
                    )
                except TimeoutError as e:
                    logger.info("time to respond is over")
                    logger.info(e)
                except Exception as e:
                    logger.info("unexpected error")
                    logger.info(e)
                else:

                    # Проверка совпадения хэша рандомной строки
                    if rnd_hash == res['rnd_hash']:
                        status = '200 OK'
                        info = ''
                        user['login'] = helpers.recover_full_login(user['login'])

                        # Меняем device_id, если он задан.
                        if "device_id" in res.keys():
                            self.device_id = res["device_id"]

                        # Получаем "голенький" логин, без протокола и хоста.
                        login = helpers.parse_skaro_login(user['login'])[1]

                        # Проверка на то, что название устройства не занято
                        if login not in self.application.AuthorizedPool.keys() \
                                or self.device_id not in self.application.AuthorizedPool[login].connections.keys():

                            # Обновляем словарь авторизованных пользователей,
                            # если с других аккаунтов пользователя это еще не было сделано
                            self.application.AuthorizedPool.setdefault(
                                login,
                                type("AuthorizedClient", (object,), {
                                    "connections": {},
                                    "data": user,
                                    "accounts": {}
                                })
                            )

                            # Сохраняем ссылку на ячейку в словаре,
                            # чтобы не искать каждый раз самого себя.
                            self.session = self.application.AuthorizedPool[login]

                            # Прогрузка данных пользователя (self.session)
                            yield self.init_self_user()

                            # Генерируем идентификатор сессии пользователя
                            self.session_id = self.generate_session_id()

                            # Добавляем авторизованное устройство в множество авторизованных устройств
                            self.session.connections[self.device_id] = self
                            # Если другие устройства небыли онлайн "до нас", то уведомляем всех о нашем входе.
                            if len(self.session.connections) == 1:
                                self.session.data["status"] = "online"
                                self.send_updates({
                                    "type": "changed_user_data",
                                    "login": self.session.data['login'],
                                    "update": {
                                        "status": "online"
                                    }
                                })
                        else:
                            status = '400 Bad Request'
                            info = 'Название устройства "%s" уже занято.' % (self.device_id,)
                    else:
                        status = '400 Bad Request'
                        info = "Неправильный ключ rnd."

                    if helpers.is_request(res):
                        req = {
                            "chain_uuid": res["chain_uuid"],
                            "type": "request",
                            "status": status,
                            "struct_type": "authorize_user"
                        }
                        if status[0:3] == '200':
                            req['session_id'] = self.session_id
                            req['device_id'] = self.device_id
                            req['user'] = helpers.filtering_user_data(copy.deepcopy(self.session.data))

                        if info:
                            req["info"] = info

                        self.write_client(req)

            else:
                self.write_info(
                    data,
                    status=status,
                    info=info
                )

    @tornado.web.asynchronous
    @tornado.gen.engine
    @validate.arguments
    def event_session_restore(self, data: DictSchema(
            type = StringValue(required=True),
            chain_uuid = StringValue(required=True),
            session_id = StringValue(required=True, max=255),
            device_id = StringValue(max=64, pattern=validate.P_DEVICE_ID)
        )):

        # Получаем данные юзера из его сессии(если она еще доступна)
        res_session = None
        for s in self.application.SessionsRestore:
            if s['session_id'] == data['session_id']:
                res_session = s.copy()
                self.application.SessionsRestore.remove(s)
                break

        if res_session is not None:
            status = '200 OK'
            info = ''
            (host, slogin) = helpers.parse_skaro_login(helpers.parse_login(res_session['login'])[1])
            try:
                cursor = yield database.getUserByLogin(slogin[1:])
            except (database.Warning, database.Error):
                logger.error("Error get user data", exc_info=True)
                user = None
            else:
                user = cursor.fetchone()

            if user:
                user['login'] = helpers.recover_full_login(user['login'])

                # Вспоминаем device_id
                if res_session["device_id"] is not None:
                    self.device_id = res_session["device_id"]

                # Получаем "голенький" логин, без протокола и хоста.
                login = helpers.parse_skaro_login(user['login'])[1]

                # Проверка на то, что название устройства не занято
                if login not in self.application.AuthorizedPool.keys() \
                or self.device_id not in self.application.AuthorizedPool[login].connections.keys():

                    # Обновляем словарь авторизованных пользователей,
                    # если с других аккаунтов пользователя это еще не было сделано
                    self.application.AuthorizedPool.setdefault(
                        login,
                        type("AuthorizedClient", (object,), {
                            "connections": {},
                            "data": user,
                            "accounts": {}
                        })
                    )

                    # Сохраняем ссылку на ячейку в словаре,
                    # чтобы не искать каждый раз самого себя.
                    self.session = self.application.AuthorizedPool[login]

                    # Прогрузка данных пользователя (self.session)
                    yield self.init_self_user()

                    # Генерируем идентификатор сессии пользователя
                    self.session_id = self.generate_session_id()

                    # Добавляем авторизованное устройство в множество авторизованных устройств
                    self.session.connections[self.device_id] = self
                    # Если другие устройства небыли онлайн "до нас", то уведомляем всех о нашем входе.
                    if len(self.session.connections) == 1:
                        self.session.data["status"] = "online"
                        self.send_updates({
                            "type": "changed_user_data",
                            "login": self.session.data['login'],
                            "update": {
                                "status": "online"
                            }
                        })
                else:
                    status = '400 Bad Request'
                    info = 'Название устройства "%s" стало занято за время вашего отключения. Выберите новое.' % (self.device_id,)
            else:
                status = '400 Bad Request'
                info = "Ошибка при попытке поиска пользователя с таким именем."
        else:
            status = '400 Bad Request'
            info = "Время ожидания сессии истекло."

        if helpers.is_request(data):
            req = {
                "chain_uuid": data["chain_uuid"],
                "type": "request",
                "status": status,
                "struct_type": "session_restore"
            }
            if status[0:3] == '200':
                req['session_id'] = self.session_id
                req['user'] = helpers.filtering_user_data(copy.deepcopy(self.session.data))

            if info:
                req["info"] = info

            self.write_client(req)

    @tornado.web.asynchronous
    @tornado.gen.engine
    @validate.arguments
    def event_update_my_data(self, data: DictSchema(
            type = StringValue(required=True),
            chain_uuid = StringValue(required=True),
            update = DictSchema(
                login = StringValue(
                    max=255,
                    pattern=validate.P_LOGIN,
                    prepare_callback=lambda x: helpers.parse_login(x, True)[2][1:]
                ),
                name = StringValue(max=255),
                email = StringValue(pattern=validate.P_MAIL, max=255),
                current_info = StringValue(max=255)
            )
        )):
        status = "200 OK"
        info = ""

        if "login" in data["update"].keys()\
                and helpers.parse_login(data["update"]["login"], True) != helpers.parse_login(self.session.data['login'], True):

            old_login = self.session.data['login']
            data["update"]['login'] = helpers.recover_full_login(data["update"]['login'])

            try:
                cursor = yield database.getSomeLogin(data["update"]['login'])
            except (database.Warning, database.Error):
                logger.error("Error check login exists", exc_info=True)
                # Информируем пользователя
                self.write_client({
                    "chain_uuid": data["chain_uuid"],
                    "type": "request",
                    "status": "500 Internal Server Error",
                    "info": "Ошибка при попытке проверки занятости логина, попробуйте еще раз",
                    "struct_type": "update_my_data"
                })
                return
            else:
                if cursor.fetchone()[0] > 0:
                    self.write_client({
                        "chain_uuid": data["chain_uuid"],
                        "type": "request",
                        "status": "400 Bad Request",
                        "info": "Этот логин уже занят",
                        "struct_type": "update_my_data"
                    })
                    return
        else:
            old_login = None
            data["update"]["login"] = self.session.data['login']

        # Если что то изменилось, обновляем данные
        if len(data["update"]) > 0:
            try:
                yield database.setUser(data["update"], search_by_login=old_login)
            except (database.Warning, database.Error):
                logger.error("Failed to update user data", exc_info=True)
                status = "500 Internal Server Error"
                info = "Ошибка при попытке изменить данные пользователя"
            else:
                # Обновление логина пользователя, если он был сменен
                if old_login is not None:
                    self.application.AuthorizedPool[
                        helpers.parse_skaro_login(data["update"]['login'])[1]
                    ] = self.session
                    del self.application.AuthorizedPool[
                        helpers.parse_skaro_login(old_login)[1]
                    ]
                else:
                    del data["update"]["login"]

                old_login = self.session.data['login']
                self.session.data.update(data["update"])

                # Отправка уведомлений об изменении данных
                send = {
                    "type": "changed_user_data",
                    "update": data["update"]
                }
                # На устройства самому себе
                for device in [x for x in self.session.connections.values() if x.device_id != self.device_id]:
                    device.write_client(send)
                # На устройства тех, у кого пользователь есть в контактах
                send["login"] = old_login
                self.send_updates(send)
        else:
            status = "400 Bad Request"
            info = "Новые данные либо пусты либо совпадают со старыми"

        req = {
            "chain_uuid": data["chain_uuid"],
            "type": "request",
            "status": status,
            "struct_type": "update_my_data"
        }
        if len(info) > 0:
            req['info'] = info

        self.write_client(req)

    @tornado.web.asynchronous
    @tornado.gen.engine
    @validate.arguments
    def event_search_contacts(self, data: DictSchema(
            type = StringValue(required=True),
            chain_uuid = StringValue(required=True),
            query = StringValue(required=True, max=255)
        )):

        status = "200 OK"
        info = ""
        res = None

        (protocol, part_of_login_in_protocol) = helpers.parse_login(data["query"])
        if protocol == "skaro":
            (host, part_of_login) = helpers.parse_skaro_login(part_of_login_in_protocol)
            self_login = helpers.parse_login(self.session.data["login"], True)[2][1:]
            if host == options.host:
                try:
                    cursor = yield database.searchContacts(part_of_login[1:], self_login)
                except (database.Warning, database.Error):
                    logger.error("Error in searchContacts('%s')" % (data["query"],), exc_info=True)
                    status = "500 Internal Server Error"
                    info = "Ошибка при попытке поиска пользователей"
                else:
                    res = cursor.fetchall()
            else:
                # поиск на другом skaro сервере
                pass
        else:
            # поиск пользователя в другом протоколле
            pass

        req = {
            "type": "request",
            "chain_uuid": data["chain_uuid"],
            "status": status,
            "struct_type": "search_contacts"
        }
        if info:
            req["info"] = info
        if res is not None:
            req["result"] = res

        self.write_client(req)

    # Добавляет пользователя в контакты
    @tornado.web.asynchronous
    @tornado.gen.engine
    @validate.arguments
    def event_add_contact(self, data: DictSchema(
            type = StringValue(required=True),
            chain_uuid = StringValue(),
            login = StringValue(
                required=True,
                max=255,
                pattern=validate.P_LOGIN,
                prepare_callback=lambda x: helpers.parse_login(x, True)[2][1:]
            ),
            send_message = StringValue()
        )):

        data['login'] = helpers.recover_full_login(data['login'])

        # проверка. может этот юзер уже является контактом
        if data['login'] in self.session.data["contacts"].keys():
            self.write_info(data,
                status="400 Bad Request",
                info="Этот пользователь или конференция уже является вашим контактом"
            )
            return

        status = '200 OK'
        info = ''

        dump = self.session.data['contacts'].copy()
        # Добавление юзера в контакты пользователя
        self.session.data['contacts'].update({data["login"]: {}})

        try:
            yield database.setUser({"contacts": self.session.data["contacts"], "login": self.session.data["login"]})
        except (database.Warning, database.Error):
            logger.error("Failed to add user to contacts", exc_info=True)
            status = "500 Internal Server Error"
            info = "Ошибка при попытке изменить данные пользователя"
            self.session.data['contacts'] = dump
        else:
            (protocol, login_in_prl) = helpers.parse_login(data['login'])
            if protocol != "skaro":
                # другие протоколы...
                # может что-то сделать нужно
                pass

        user = None
        if status[:3] == "200":
            user = yield self.get_user_data(data['login'])
            if not user:
                status = "400 Bad Request"
                info = "Ошибка при попытке получить данные пользователя"
            else:
                # информирование других устройств
                self.send_updates({
                    "type": "changed_contacts",
                    "add_contacts": {data["login"]: {}}
                }, users_for_update=set(), send_self_devices=True)

        if helpers.is_request(data):
            req = {
                "chain_uuid": data["chain_uuid"],
                "type": "request",
                "status": status,
                "struct_type": "add_contact"
            }
            if user:
                req['user'] = user
            if len(info) > 0:
                req['info'] = info

            self.write_client(req)

    # Удаляет пользователя из контактов
    @tornado.web.asynchronous
    @tornado.gen.engine
    @validate.arguments
    def event_del_contact(self, data: DictSchema(
            type = StringValue(required=True),
            chain_uuid = StringValue(),
            login = StringValue(
                required=True,
                max=255,
                pattern=validate.P_LOGIN,
                prepare_callback=lambda x: helpers.parse_login(x, True)[2][1:]
            )
        )):

        dump = self.session.data['contacts'].copy()
        data['login'] = helpers.recover_full_login(data['login'])

        # Удаляем пользоватея из контактов
        is_contact = False
        if data["login"] in self.session.data['contacts'].keys():
            del self.session.data['contacts'][data["login"]]
            is_contact = True

        if not is_contact:
            if helpers.is_request(data):
                self.write_client({
                    "chain_uuid": data["chain_uuid"],
                    "type": "request",
                    "status": "400 Bad Request",
                    "info": "Пользователь не является вашим контактом",
                    "struct_type": "del_contact"
                })
            else:
                self.write_client({
                    "type": "error",
                    "status": "400 Bad Request",
                    "info": "Пользователь не является вашим контактом",
                    "struct_type": "del_contact"
                })

        status = '200 OK'
        info = ''

        try:
            yield database.setUser({"contacts": self.session.data["contacts"], "login": self.session.data["login"]})
        except (database.Warning, database.Error):
            logger.error("Failed to delete user from contacts", exc_info=True)
            status = "500 Internal Server Error"
            info = "Ошибка при попытке изменить данные пользователя"
            self.session.data['contacts'] = dump
        else:
            (protocol, login_in_protocol) = helpers.parse_login(data['login'])
            if protocol != "skaro":
                # другие протоколы...
                # может что то сделать нужно
                pass

        if status[:3] == "200":
            # Очистка общей истории сообщений
            is_clear = yield self.clear_history(data['login'])
            if is_clear is False:
                self.write_client({
                    "type": "error",
                    "status": "500 Internal Server Error",
                    "info": "Не удалось очистить историю переписки с пользователем %s" % (data['login'],)
                })
            else:
                # информирование других устройств
                self.send_updates({
                    "type": "changed_contacts",
                    "del_contacts": {data["login"]: {}}
                }, users_for_update=set(), send_self_devices=True)

        if helpers.is_request(data):
            req = {
                "chain_uuid": data["chain_uuid"],
                "type": "request",
                "status": status,
                "struct_type": "del_contact"
            }
            if len(info) > 0:
                req['info'] = info

            self.write_client(req)

    @tornado.web.asynchronous
    @tornado.gen.engine
    @validate.arguments
    def event_get_contacts(self, data: DictSchema(
            type = StringValue(required=True),
            chain_uuid = StringValue(required=True)
        )):

        contacts = yield self.get_self_contacts()

        if contacts is not False:
            status = '200 OK'
            info = ''
        else:
            contacts = None
            status = "500 Internal Server Error"
            info = "Ошибка при попытке получить контакты"

        req = {
            "chain_uuid": data["chain_uuid"],
            "type": "request",
            "status": status,
            "struct_type": "get_contacts"
        }
        if info:
            req['info'] = info

        if contacts is not None:
            req['data'] = contacts

        self.write_client(req)

    @tornado.web.asynchronous
    @tornado.gen.engine
    @validate.arguments
    def event_get_user_data(self, data: DictSchema(
            type = StringValue(required=True),
            chain_uuid = StringValue(required=True),
            login = StringValue(
                required=True,
                max=255,
                pattern=validate.P_LOGIN,
                prepare_callback=lambda x: helpers.parse_login(x, True)[2][1:]
            )
        )):

        user = yield self.get_user_data(data['login'])

        if user:
            status = '200 OK'
            info = ''
        else:
            user = False
            status = "400 Bad Request"
            info = "Ошибка при попытке получить данные пользователя,\
                    возможно такого пользователя не существует"

        req = {
            "chain_uuid": data["chain_uuid"],
            "type": "request",
            "status": status,
            "struct_type": "get_user_data"
        }
        if info:
            req['info'] = info
        if user:
            req['data'] = user

        self.write_client(req)
