#!/usr/bin/env python
# Skaro is the open standard for instant messaging
# Copyright © 2017 Exarh Team
#
# This file is part of skaro-server.
#
# Skaro-server is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging

# Tornado server
from tornado.ioloop import IOLoop
import tornado.web
import tornado.gen

# Local modules and packages
from modules import helpers, validate
from modules.validate import DictSchema, IntValue, StringValue, DictValue

logger = logging.getLogger(__name__)  # named logging


class RequestsManagement:
    """
    @DynamicAttrs - for understanding attributes in base class and other mixins (for PyCharm)

    Storage for callbacks and e.t.c
    Example:
        <request id>: {
            "callback": <function callback>,
            "timer": <tornado.ioloop._Timeout object>,
            "checker_extra": <validate.DictSchema and other checkers>
        },
        ...
    """

    sent_requests = {}


    @tornado.web.asynchronous
    @tornado.gen.engine
    @validate.arguments
    def event_request(self, data: DictSchema(
            DictSchema.ALLOW_MORE_KEYS,
            type = StringValue(required=True),
            chain_uuid = StringValue(required=True),
            status = StringValue(
                required=True,
                max=255,
                pattern=validate.P_STATUS
            ),
            info = StringValue(max=500)
        )):
        if data['chain_uuid'] in self.sent_requests.keys():
            request_data = self.sent_requests[data['chain_uuid']]

            if request_data["checker_extra"] is not None:
                err = request_data["checker_extra"].check_data(
                    {k:v for k,v in data.items() if k not in ("type", "chain_uuid", "status", "info")}
                )
                if err:
                    self.write_info({}, info=err)
                    return

            IOLoop.instance().remove_timeout(request_data["timer"])
            request_data["callback"](data)
            del self.sent_requests[data['chain_uuid']]
        else:
            self.write_client({
                "type": "error",
                "status": "400 Request Timeout",
                "info": "Не найден обработчик ответа, либо вышло время на его вызов и он был удален"
            })

    @helpers.TaskWrapper
    def send_request(self, data, checker_extra=None, timeout=10, callback=None):
        """Sending message with field 'chain_uuid' for waiting answer from client and sets callback

        Example:
            try:
                res = yield self.send_request({
                        "type": "request",
                        "chain_uuid": <auto>,
                        "status": "200 Ok",
                        "info": "zer good"
                    },
                    DictSchema(
                        public_key=StringValue(required=True)
                    ),
                    timeout=100 <in seconds>
                )
            except TimeoutError as e:
                logger.info("time to respond is over")
                logger.info(e)
            except Exception as e:
                logger.info("unexpected error")
                logger.info(e)
            else:
                logger.info("all is well")
                logger.info(res)

        """

        if 'chain_uuid' not in data:
            data['chain_uuid'] = helpers.getUUID(self.sent_requests)

        def raise_error():
            self.write_client({
                "type": "error",
                "status": "408 Request Timeout",
                "info": "Вышло время ожидания ответа клиента"
            })
            del self.sent_requests[data['chain_uuid']]
            raise TimeoutError("Time is over")

        # sets timeout for calling exception. Needed for anti-freezes.
        timer = IOLoop.instance().call_later(timeout, raise_error)

        self.sent_requests[data['chain_uuid']] = {
            "callback": callback,
            "timer": timer,
            "checker_extra": checker_extra
        }

        self.write_client(data)
