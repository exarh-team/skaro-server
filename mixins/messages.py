#!/usr/bin/env python
# Skaro is the open standard for instant messaging
# Copyright © 2017 Exarh Team
#
# This file is part of skaro-server.
#
# Skaro-server is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import logging

# for timing
from datetime import datetime, timedelta

# Tornado server
import tornado.escape
import tornado.util
import tornado.ioloop
import tornado.web
import tornado.gen
import tornado.websocket
from tornado.options import options

# Local modules and packages
from modules import config, database, helpers, validate
from modules.validate import DictSchema, IntValue, BoolValue, StringValue

logger = logging.getLogger(__name__)

class MessManagement:
    """
    @DynamicAttrs - for understanding attributes in base class and other mixins (for PyCharm)
    """
    # Осуществляет отправку сообщения
    @tornado.gen.coroutine
    def sys_send_message(self, data, hidden_for=None):
        mid, status, info = (None, '200 OK', [])
        date = str(datetime.today().strftime('%Y-%m-%d %H:%M:%S'))

        # Формируем данные для отправки сообщения
        mess = {
            "type": "received_message",
            "date": date,
            "from": {
                "name": self.session.data['name'],
                "login": self.session.data['login'],
            },
            "body": data["body"]
        }

        data["date"] = date
        data["from"] = self.session.data['login']

        # Парсинг логина перед отправкой сообщения
        (protocol, login_in_prl) = helpers.parse_login(data['to'])
        # Стандартный протокол skaro
        if protocol == 'skaro':

            (host, login) = helpers.parse_skaro_login(login_in_prl)
            if host == options.host:
                try:
                    cursor = yield database.getUserByLogin(login[1:])
                except (database.Warning, database.Error):
                    status = "500 Internal Server Error"
                    info.append("Не удалось проверить существование пользователя")
                    logger.error("Error checking user login", exc_info=True)
                else:
                    if not cursor.fetchone():
                        status = "404 Bad request"
                        info.append("Такого пользователя не существует")

            if status[:3] == "200":
                # Добавляем сообщение в историю
                try:
                    cursor = yield database.addHistory(data, hidden_for)
                except (database.Warning, database.Error):
                    status = "250 Done with errors"
                    info.append("Не удалось записать в историю")
                    logger.error("Error adding history", exc_info=True)
                else:
                    mid = mess["mid"] = cursor.fetchone()[0]

                # Поиск контакта среди авторизованных устройств при применения изменений в рамках сессии
                user_data = self.application.AuthorizedPool.get(
                    helpers.parse_skaro_login(data["to"])[1]
                )

                if user_data is not None:
                    # Добавляем себя в контакты адресату, если вы еще не являетесь его контактом
                    if self.session.data["login"] not in user_data.data["contacts"]:
                        user_data.data["contacts"][self.session.data["login"]] = {}
                        try:
                            yield database.setUser({
                                "contacts": user_data.data["contacts"],
                                "login": user_data.data["login"]
                            })
                        except (database.Warning, database.Error):
                            status = "250 Done with errors"
                            info.append("Не удалось обновить список контактов ответчика (%s)" % (user_data.data["login"],))
                            logger.error("Error updating contact list (db)", exc_info=True)

                    for waiter in user_data.connections.values():
                        waiter.write_client(mess)

                # Отправляем свое сообщение на собственные устройства, которые тоже подключены
                mess["to"] = helpers.recover_full_login(data['to'])
                for device_id, waiter in self.session.connections.items():
                    if device_id != self.device_id:
                        waiter.write_client(mess)

        # Иной протокол
        else:
            if protocol not in self.session.accounts.keys():
                status = "403 Forbidden"
                info.append("Не удалось отправить сообщение. Вам необходимо иметь аккаунт \"%s\"." % (protocol,))
            else:
                cls = self.session.accounts[protocol]
                try:
                    if cls.send_message(data["to"], data["body"]) is False:
                        raise Exception('send_message method returned False')
                except Exception:
                    status = "500 Internal Server Error"
                    info.append("Не удалось отправить сообщение. Ошибка в модуле обработчике протокола.")
                    logger.error("Error sending message", exc_info=True)

            if status[:3] == "200":
                # Добавляем сообщение в историю
                try:
                    cursor = yield database.addHistory(data, hidden_for)
                except (database.Warning, database.Error):
                    status = "250 Done with errors"
                    info.append("Не удалось записать в историю")
                    logger.error("Error adding history", exc_info=True)
                else:
                    mid = mess["mid"] = cursor.fetchone()[0]

        # Если ответчик не в контактах, то добавляем его в контакты
        if data["to"] not in self.session.data["contacts"].keys():

            self.session.data["contacts"].update({data["to"]: {}})
            try:
                yield database.setUser({"contacts": self.session.data["contacts"], "login": self.session.data["login"]})
            except (database.Warning, database.Error):
                logger.error("Error update contacts", exc_info=True)
                # Информируем пользователя
                self.write_client({
                    "type": "error",
                    "status": "500 Internal Server Error",
                    "info": "Ошибка при попытке обновить список контактов пользователя",
                })

        return (mid, status, info)

    # Удаляет историю
    @tornado.gen.coroutine
    def clear_history(self, login):
        try:
            yield database.clearHistory(self.session.data["login"], login)
        except (database.Warning, database.Error):
            logger.error("Error database.clearHistory(%s, %s)", self.session.data["login"], login, exc_info=True)
            return False
        else:
            return True

    @tornado.web.asynchronous
    @tornado.gen.engine
    @validate.arguments
    def event_get_history(self, data: DictSchema(
            type = StringValue(required=True),
            chain_uuid = StringValue(required=True),
            login = StringValue(
                required=True,
                max=255,
                pattern=validate.P_LOGIN,
                prepare_callback=lambda x: helpers.parse_login(x, True)[2][1:]
            )
        )):

        status = '200 OK'
        info = ''

        # is_conf - дополнительный аргумент, привилигированного выбора типа сообщения
        # особенно актуально для сторонних протоколов
        is_conf = data.pop("is_conference", None)

        # Определяем вид сообщений(в контексте конференции или лички)
        if (is_conf is not None and is_conf is True) or helpers.is_skaro_conf_login(data["login"]):
            is_conf = True
        else:
            is_conf = False

        messages = False
        try:
            cursor = yield database.getHistory(data['login'], self.session.data['login'], is_conference=is_conf)
        except (database.Warning, database.Error):
            status = '500 Internal Server Error'
            info = sys.exc_info()[0]
            logger.error("Error getting history", exc_info=True)
        else:
            messages = cursor.fetchall()

        if not messages:
            messages = []

        # TODO: Плохо, но не придумал пока как лучше сделать.
        for i, msg in enumerate(messages):
            messages[i] = dict(messages[i])
            # Настройка для json формата
            messages[i]["date"] = msg["date"].strftime('%Y-%m-%d %H:%M:%S')

            messages[i]["from"] = {
                "name": msg["sent"],  # TODO: нужно выводить имя, а не логин(усовершенствовать запрос надо)
                "login": msg["sent"]
            }

            if is_conf:
                messages[i]["from"]["in_conference"] = helpers.recover_full_login(data["login"], with_symbol="~")

            del messages[i]["sent"]
            del messages[i]["took"]

        req = {
            "chain_uuid": data["chain_uuid"],
            "type": "request",
            "history": messages,
            "status": status,
            "struct_type": "get_history"
        }
        if info:
            req['info'] = info

        self.write_client(req)

    # Очистка общей истории сообщений
    @tornado.web.asynchronous
    @tornado.gen.engine
    @validate.arguments
    def event_clear_history(self, data: DictSchema(
            type = StringValue(required=True),
            chain_uuid = StringValue(),
            login = StringValue(
                required=True,
                max=255,
                pattern=validate.P_LOGIN,
                prepare_callback=lambda x: helpers.parse_login(x, True)[2][1:]
            )
        )):

        is_clear = yield self.clear_history(data['login'])
        if is_clear is True:
            if helpers.is_request(data):
                self.write_client({
                    "chain_uuid": data["chain_uuid"],
                    "type": "request",
                    "status": "200 OK",
                    "struct_type": "clear_history"
                })
        else:
            if helpers.is_request(data):
                self.write_client({
                    "chain_uuid": data["chain_uuid"],
                    "type": "request",
                    "status": "500 Internal Server Error",
                    "info": "Не удалось очистить историю переписки",
                    "struct_type": "clear_history"
                })
            else:
                self.write_client({
                    "type": "error",
                    "status": "500 Internal Server Error",
                    "info": "Не удалось очистить историю переписки с пользователем %s" % (data['login'],)
                })

    @tornado.web.asynchronous
    @tornado.gen.engine
    @validate.arguments
    def event_send_message(self, data: DictSchema(
            type = StringValue(required=True),
            chain_uuid = StringValue(),
            to = StringValue(
                required=True,
                max=255,
                pattern=validate.P_LOGIN,
                prepare_callback=lambda x: helpers.parse_login(x, True)[2][1:]
            ),
            body = StringValue(
                required=True,
                max=1000
            ),
            is_conference = BoolValue(required=False)
        )):

        # status = '200 OK'
        # info = []

        # is_conference - дополнительный аргумент, привилигированного выбора типа сообщения
        # особенно актуально для сторонних протоколов
        is_conf = data.pop("is_conference", None)

        # Определяем вид сообщения(в контексте конференции или лички)
        if (is_conf is None or is_conf is False) and helpers.is_skaro_login(data["to"]):
            is_conf = False
            (mid, status, info) = yield self.sys_send_message(data)
        else:
            is_conf = True
            (mid, status, info) = yield self.sys_send_conf_message(data)

        # Информируем, что сервер принял сообщение и обработал его.
        if helpers.is_request(data):
            req = {
                "chain_uuid": data["chain_uuid"],
                "type": "request",
                "date": data['date'],  # added in self.sys_send_(conf)_message(data)
                "status": status,
                "struct_type": "send_message"
            }
            if len(info) > 0:
                req['status'] = '500 Internal Server Error'
                req['info'] = "\n".join(info)
            else:
                req["mid"] = mid

            self.write_client(req)
