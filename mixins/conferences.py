#!/usr/bin/env python
# Skaro is the open standard for instant messaging
# Copyright © 2017 Exarh Team
#
# This file is part of skaro-server.
#
# Skaro-server is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging

# for timing
from datetime import datetime

# Tornado server
import tornado.escape
import tornado.util
import tornado.ioloop
import tornado.web
import tornado.gen
import tornado.websocket
from tornado.options import options

# Local modules and packages
from modules import config, database, helpers, validate
from modules.validate import DictSchema, IntValue, StringValue, DictValue


logger = logging.getLogger(__name__)

class ConfManagement:
    """
    @DynamicAttrs - for understanding attributes in base class and other mixins (for PyCharm)
    """

    # Изменился список участников конференции(в которой self юзер уже является участником)
    def sysevent_changed_conference_members(self, data):
        self.write_client(data)

    # Изменились данные конферении
    def sysevent_changed_conference(self, data):
        self.write_client(data)

    # Возвращает данные о юзере
    # вызов только через user_data = yield self.get_conf_data(login)
    @tornado.gen.coroutine
    def get_conf_data(self, full_login):
        conf = False
        (protocol, login_in_prl) = helpers.parse_login(full_login)
        if protocol == "skaro":
            (host, login) = helpers.parse_skaro_login(login_in_prl)

            # TODO: Если контакт - не конференция
            if login[0] == "@":
                return False  # Аналогично raise tornado.gen.Return(False)

            if host == options.host:
                try:
                    cursor = yield database.getConference(login[1:])
                except (database.Warning, database.Error):
                    logger.error("Error in get conference data", exc_info=True)
                    conf = False
                else:
                    conf = cursor.fetchone()
                    if conf:
                        conf = helpers.filtering_conf_data(dict(conf))
                        conf['login'] = helpers.recover_full_login(conf['login'], with_symbol="~")
                    else:
                        conf = False
            else:
                # Обращение к другому skaro серверу...
                pass
        else:
            conf = self.session.accounts[protocol].get_conf_data(login_in_prl)
            if conf:
                conf = helpers.filtering_conf_data(conf)

        return conf  # Аналогично raise tornado.gen.Return(user)

    # Осуществляет отправку сообщения
    @tornado.gen.coroutine
    def sys_send_conf_message(self, data, hidden_for=None):
        mid, status, info = (None, '200 OK', [])
        date = str(datetime.today().strftime('%Y-%m-%d %H:%M:%S'))

        conf_login = helpers.recover_full_login(data["to"], with_symbol="~")

        # Формируем данные для отправки сообщения
        mess = {
            "type": "received_message",
            "date": date,
            "from": {
                "name": self.session.data['name'],
                "login": self.session.data['login'],
                "in_conference": conf_login
            },
            "body": data["body"]
        }

        data["date"] = date
        data["from"] = self.session.data['login']

        # Парсинг логина перед отправкой сообщения
        (protocol, login_in_prl) = helpers.parse_login(conf_login, to_recover_with="~")
        # Стандартный протокол skaro
        if protocol == 'skaro':

            # Проверка на существование конфы
            try:
                cursor = yield database.checkConferenceExists(conf_login)
            except (database.Warning, database.Error):
                status = "500 Internal Server Error"
                info.append("Не удалось проверить существование такой конференции")
                logger.error("Error checking conference login", exc_info=True)
            else:
                if not cursor.fetchone()[0]:
                    status = "404 Bad request"
                    info.append("Такой конференции не существует")
                else:
                    # Добавляем сообщение в историю
                    try:
                        cursor = yield database.addHistory(data, is_conference=True)
                    except (database.Warning, database.Error):
                        status = "250 Done with errors"
                        info.append("Не удалось записать в историю")
                        logger.error("Error adding history", exc_info=True)
                    else:
                        mid = mess["mid"] = cursor.fetchone()[0]

                    # Получение списка участников конференции
                    conf_data = None
                    try:
                        cursor = yield database.getConference(conf_login)
                    except (database.Warning, database.Error):
                        status = "500 Internal Server Error"
                        info.append("Не удалось получить список участников конференции")
                        logger.error("Error getting conference members", exc_info=True)
                    else:
                        conf_data = cursor.fetchone()

                    # Проверки прав доступа
                    is_send = False
                    if conf_data:
                        is_send = True

                        # Проверка на существование отправителя в списке участников конференции
                        my_member_data = conf_data["members"].get(self.session.data["login"], None)
                        if my_member_data is None:
                            status = "405 Not Allowed"
                            info.append("Вы не являетесь участником конференции")
                            is_send = False

                        # Проверка, что отправитель не Гость. Гостям запрещено отправлять сообщения
                        if is_send:
                            my_group = my_member_data.get("group", "quest")
                            if my_group == "quest":
                                status = "405 Not Allowed"
                                info.append("Группе \"%s\" запрещено отправлять сообщения в конференцию".format(my_group))
                                is_send = False

                    # Отправка
                    if is_send:
                        # поиск участников конференции онлайн
                        for member_login, member_info in conf_data["members"].items():
                            user_data = self.application.AuthorizedPool.get(
                                helpers.parse_skaro_login(member_login)[1]
                            )

                            if user_data is not None:
                                # Добавляем конференцию в контакты,
                                # если у участника конференции она еще не в контакатах
                                if conf_login not in user_data.data["contacts"]:
                                    user_data.data["contacts"][conf_login] = {"type":"conference"}
                                    try:
                                        yield database.setUser({
                                            "contacts": user_data.data["contacts"],
                                            "login": user_data.data["login"]
                                        })
                                    except (database.Warning, database.Error):
                                        status = "250 Done with errors"
                                        info.append("Не удалось обновить список контактов ответчика (%s)" % (user_data.data["login"],))
                                        logger.error("Error updating contact list (db)", exc_info=True)

                                for waiter in user_data.connections.values():
                                    # непоср. отправка
                                    waiter.write_client(mess)

                            # Участник не в онлайне
                            else:
                                # Делаем что-то, или не делаем...
                                pass

                        # Отправляем свое сообщение на собственные устройства, которые тоже подключены
                        mess["to"] = conf_login
                        for device_id, waiter in self.session.connections.items():
                            if device_id != self.device_id:
                                waiter.write_client(mess)


        # Иной протокол
        else:
            if protocol not in self.session.accounts.keys():
                status = "403 Forbidden"
                info.append("Не удалось отправить сообщение. Вам необходимо иметь аккаунт \"%s\"." % (protocol,))
            else:
                cls = self.session.accounts[protocol]
                try:
                    if cls.send_conf_message(data["to"], data["body"]) is False:
                        raise Exception('send_message method returned False')
                except Exception:
                    status = "500 Internal Server Error"
                    info.append("Не удалось отправить сообщение. Ошибка в модуле обработчике протокола.")
                    logger.error("Error sending message", exc_info=True)

            if status[:3] == "200":
                # Добавляем сообщение в историю
                try:
                    cursor = yield database.addHistory(data, is_conference=True)
                except (database.Warning, database.Error):
                    status = "250 Done with errors"
                    info.append("Не удалось записать в историю")
                    logger.error("Error adding history", exc_info=True)
                else:
                    mid = mess["mid"] = cursor.fetchone()[0]

        # Если ответчик не в контактах, то добавляем его в контакты
        if data["to"] not in self.session.data["contacts"].keys():

            self.session.data["contacts"].update({data["to"]: {"type": "conference"}})
            try:
                yield database.setUser({"contacts": self.session.data["contacts"], "login": self.session.data["login"]})
            except (database.Warning, database.Error):
                logger.error("Error update contacts", exc_info=True)
                # Информируем пользователя
                self.write_client({
                    "type": "error",
                    "status": "500 Internal Server Error",
                    "info": "Ошибка при попытке обновить список контактов пользователя",
                })

        return (mid, status, info)

    @tornado.web.asynchronous
    @tornado.gen.engine
    @validate.arguments
    def event_add_conference(self, data: DictSchema(
            type = StringValue(required=True),
            chain_uuid = StringValue(),
            login = StringValue(
                required=True,
                max=255,
                pattern=validate.P_LOGIN,
                prepare_callback=lambda x: helpers.parse_login(x, True)[2][1:]
            ),
            name = StringValue(max=255),
            description = StringValue(max=255),
            members = DictValue(
                keys = StringValue(
                    required=True,
                    max=255,
                    pattern=validate.P_LOGIN,
                    prepare_callback=lambda x: helpers.parse_login(x, True)[2][1:]
                ),
                values = DictSchema(
                    group = StringValue(
                        pattern=validate.compile(r"^[\w\d_]+$", flags=validate.IGNORECASE | validate.UNICODE)
                    )
                )
            ),
            mode = StringValue(
                pattern=validate.compile(r"^[\w\d_]+$", flags=validate.IGNORECASE | validate.UNICODE)
            ),
            password = StringValue(max=255)
        )):

        # Получаем полноценный логин, если он был задан не полностью
        (protocol, login_in_prl) = helpers.parse_login(data["login"], to_recover_with="~")
        data["login"] = helpers.recover_full_login(data["login"], with_symbol="~")

        # Добавление конференции
        if protocol == "skaro":
            (host, skaro_login) = helpers.parse_skaro_login(login_in_prl)
            if host == options.host:
                # Проверка на существование конференции с такимже логином
                try:
                    cursor = yield database.checkConferenceExists(skaro_login)
                except (database.Warning, database.Error):
                    logger.error("Error checking conference exists", exc_info=True)
                    self.write_info(data,
                        status="500 Internal Server Error",
                        info="Не удалось проверить существование конференции"
                    )
                    return
                else:
                    if cursor.fetchone()[0] > 0:
                        self.write_info(data,
                            status="400 Bad Request",
                            info="Этот логин конференции уже занят"
                        )
                        return

                # Выставляем права админа автору конференции
                data.setdefault("members", {})[self.session.data["login"]] = {"group": "admin"}

                try:
                    yield database.addConference(data.copy())
                except (database.Warning, database.Error):
                    logger.error("Error adding conference", exc_info=True)
                    self.write_info(data,
                        status="500 Internal Server Error",
                        info="Не удалось добавить конференцию"
                    )
                    return
            else:
                # todo: другие skaro серверы
                pass
        else:
            # todo: создание конференции в других протоколах
            pass

        # Добавляем конференцию в контакты
        self.session.data['contacts'].update({data["login"]: {"type": "conference"}})

        try:
            yield database.setUser({"contacts": self.session.data["contacts"], "login": self.session.data["login"]})
        except (database.Warning, database.Error):
            logger.error("Failed to add conference to contacts", exc_info=True)
            del self.session.data['contacts'][data["login"]]
            self.write_info(data,
                status="500 Internal Server Error",
                info="Ошибка при попытке изменить данные пользователя"
            )
            if protocol == "skaro" and host == options.host:
                try:
                    yield database.delConference(data["login"])
                except (database.Warning, database.Error):
                    logger.error("Failed to remove added conference (with errors)", exc_info=True)
                    self.write_client({
                        "type": "error",
                        "status": "500 Internal Server Error",
                        "info": "Не удалось удалить добавленную с ошибками конференцию"
                    })
            return
        else:
            if protocol != "skaro":
                # другие протоколы...
                # может что то сделать нужно
                pass

        if "members" in data.keys():
            # Добавляем новым участникам конференции конференцию в контакты
            try:
                yield database.addContactUsersByLogins(data["members"].keys(), {
                    data["login"]: {
                        "type": "conference"
                    }
                })
            except (database.Warning, database.Error):
                logger.error("Error adding conference in contacts new members", exc_info=True)
                self.write_client({
                    "type": "error",
                    "status": "500 Internal Server Error",
                    "info": "Не удалось добавить конференцию в контакты её участникам"
                })
                # todo: тут нужно удалять участников, которым не удалось добавить конфу в контакты
            else:
                self.send_updates({
                    "type": "changed_contacts",
                    "add_contacts": {data["login"]: {"type": "conference"}}
                }, users_for_update=data["members"].keys(), send_self_devices=True)

        # Операция прошла успешно
        if helpers.is_request(data):
            self.write_client({
                "chain_uuid": data["chain_uuid"],
                "type": "request",
                "status": "200 OK",
                "struct_type": "add_conference"
            })

    @tornado.web.asynchronous
    @tornado.gen.engine
    @validate.arguments
    def event_update_conference(self, data: DictSchema(
            type = StringValue(required=True),
            chain_uuid = StringValue(),
            login = StringValue(
                required=True,
                max=255,
                pattern=validate.P_LOGIN,
                prepare_callback=lambda x: helpers.parse_login(x, True)[2][1:]
            ),
            update = DictSchema(
                name = StringValue(max=255),
                description = StringValue(max=255),
                mode = StringValue(
                    pattern=validate.compile(r"^[\w\d_]+$", flags=validate.IGNORECASE | validate.UNICODE)
                ),
                password = StringValue(max=255)
            )
        )):

        # Получаем полноценный логин, если он был задан не полностью
        (protocol, login_in_prl) = helpers.parse_login(data["login"], to_recover_with="~")
        data["login"] = helpers.recover_full_login(data["login"], with_symbol="~")

        # Обновление конференции
        if protocol == "skaro":
            (host, skaro_login) = helpers.parse_skaro_login(login_in_prl)
            if host == options.host:
                # Получение настроек конференции
                try:
                    cursor = yield database.getConference(skaro_login)
                except (database.Warning, database.Error):
                    logger.error("Error getting conference", exc_info=True)
                    self.write_info(data,
                        status="500 Internal Server Error",
                        info="Не удалось получить текущие данные конференции"
                    )
                    return
                else:
                    conf = cursor.fetchone()
                    if not len(conf):
                        self.write_info(data,
                            status="400 Bad Request",
                            info="Такой конференции не существует"
                        )
                        return

                # Проверяем права доступа
                access_error = []
                my_group = conf["members"].get(self.session.data["login"], {}).get("group", "quest")
                conf["login"] = helpers.recover_full_login(conf["login"], with_symbol="~")

                if my_group in ("moderator", "member", "quest"):
                    if "mode" in data["update"].keys() and data["update"]["mode"] != conf["mode"]:
                        access_error.append("Группе \"%s\" запрещено изменять режим конференции".format(my_group))
                    if "password" in data["update"].keys() and data["update"]["password"] != conf["password"]:
                        access_error.append("Группе \"%s\" запрещено изменять/устанавливать пароль конференции"
                                            .format(my_group))

                if my_group in ("member", "quest"):
                    if "name" in data["update"].keys() and data["update"]["name"] != conf["name"]:
                        access_error.append("Группе \"%s\" запрещено изменять название конференции".format(my_group))
                    if "description" in data["update"].keys() and data["update"]["description"] != conf["description"]:
                        access_error.append("Группе \"%s\" запрещено изменять описание конференции".format(my_group))

                if len(access_error):
                    self.write_info(data,
                        status="405 Not Allowed",
                        info="\n".join(access_error)
                    )
                    return

                # Обновляем данные конференции.
                try:
                    yield database.setConference(data["update"].copy(), search_by_login=data["login"])
                except (database.Warning, database.Error):
                    logger.error("Error updating conference", exc_info=True)
                    self.write_info(data,
                        status="500 Internal Server Error",
                        info="Не удалось обновить данные конференции"
                    )
                    return
            else:
                # другие skaro серверы
                conf = {}
                pass
        else:
            # создание конференции в других протоколах
            conf = {}
            pass

        # Извещаем пользователей о изменении конференции
        self.send_updates({
            "type": "changed_conference",
            "login": data["login"],
            "update": data["update"]
        }, for_conference=data["login"])

        # Операция прошла успешно
        if helpers.is_request(data):
            self.write_client({
                "chain_uuid": data["chain_uuid"],
                "type": "request",
                "status": "200 OK",
                "struct_type": "update_conference"
            })

    @tornado.web.asynchronous
    @tornado.gen.engine
    @validate.arguments
    def event_change_conference_members(self, data: DictSchema(
            type=StringValue(required=True),
            chain_uuid=StringValue(required=True),
            login=StringValue(
                required=True,
                max=255,
                pattern=validate.P_LOGIN,
                prepare_callback=lambda x: helpers.parse_login(x, True)[2][1:]
            ),
            add_members=DictValue(
                keys = StringValue(
                    required=True,
                    max=255,
                    pattern=validate.P_LOGIN,
                    prepare_callback=lambda x: helpers.parse_login(x, True)[2][1:]
                ),
                values = DictSchema(
                    group = StringValue(
                        pattern=validate.compile(r"^[\w\d_]+$", flags=validate.IGNORECASE | validate.UNICODE)
                    )
                )
            ),
            del_members=DictValue(
                keys = StringValue(
                    required=True,
                    max=255,
                    pattern=validate.P_LOGIN,
                    prepare_callback=lambda x: helpers.parse_login(x, True)[2][1:]
                ),
                values = DictSchema(
                    reason = StringValue()
                )
            ),
            update_members=DictValue(
                keys = StringValue(
                    required=True,
                    max=255,
                    pattern=validate.P_LOGIN,
                    prepare_callback=lambda x: helpers.parse_login(x, True)[2][1:]
                ),
                values = DictSchema(
                    group = StringValue(
                        pattern=validate.compile(r"^[\w\d_]+$", flags=validate.IGNORECASE | validate.UNICODE)
                    )
                )
            )
        )):

        if "update_members" not in data.keys()\
        and "add_members" not in data.keys()\
        and "del_members" not in data.keys():
            self.write_info(data,
                status="400 Bad Request",
                info="Нужно заполнить хотябы одно из полей: add_members, del_members, update_members"
            )
            return

        # Получаем полноценный логин, если он был задан не полностью
        (protocol, login_in_prl) = helpers.parse_login(data["login"], to_recover_with="~")
        data["login"] = helpers.recover_full_login(data["login"], with_symbol="~")

        # Обновление конференции
        if protocol == "skaro":
            (host, skaro_login) = helpers.parse_skaro_login(login_in_prl)
            if host == options.host:
                # Получение настроек конференции
                try:
                    cursor = yield database.getConference(skaro_login)
                except (database.Warning, database.Error):
                    logger.error("Error getting conference", exc_info=True)
                    self.write_info(data,
                        status="500 Internal Server Error",
                        info="Не удалось получить текущие данные конференции"
                    )
                    return
                else:
                    conf = cursor.fetchone()
                    if not len(conf):
                        self.write_info(data,
                            status="400 Bad Request",
                            info="Такой конференции не существует"
                        )
                        return

                # Проверяем права доступа
                access_error = []
                new_members_list = conf["members"]
                my_group = conf["members"].get(self.session.data["login"], {}).get("group", "quest")
                conf["login"] = helpers.recover_full_login(conf["login"], with_symbol="~")

                if len(data.get("add_members", {})):
                    # Восстанавливаем неполные логины
                    data["add_members"] = {helpers.recover_full_login(m_l): m_i
                                                for m_l, m_i in data["add_members"].items()}

                    for member_login, member_info in data["add_members"].items():

                        new_group = member_info.get("group", "quest")

                        if member_login in conf["members"].keys():
                            access_error.append("Такой участник уже существует")
                            break

                        if my_group in ("moderator", "member", "quest"):
                            if new_group in ("admin", "moderator"):
                                access_error.append("Группе \"%s\" запрещено назначать группу \"%s\""
                                                    .format(my_group, new_group))
                                break

                        if my_group == "quest":
                            pass

                    new_members_list = helpers.recursive_merge(conf["members"], data["add_members"])

                if len(data.get("del_members", {})):
                    # Восстанавливаем неполные логины
                    data["del_members"] = {helpers.recover_full_login(m_l): m_i
                                                for m_l, m_i in data["del_members"].items()}

                    for member_login, member_info in data["del_members"].items():
                        old_group = conf["members"].get(member_login, {}).get("group", "quest")

                        if member_login not in conf["members"].keys():
                            access_error.append("Такой участник не существует")
                            break

                        if my_group in ("moderator", "member", "quest"):
                            if old_group in ("admin", "moderator"):
                                access_error.append("Группе \"%s\" запрещено удалять участников из группы \"%s\""
                                                    .format(my_group, old_group))
                                break

                        if my_group in ("member", "quest"):
                            access_error.append("Группе \"%s\" запрещено удалять участников"
                                                .format(my_group))
                            break

                    new_members_list = {m_l: m_i
                                            for m_l, m_i in conf["members"].items()
                                                if m_l not in data["del_members"].keys()}

                if len(data.get("update_members", {})):
                    # Восстанавливаем неполные логины
                    data["update_members"] = {helpers.recover_full_login(m_l): m_i
                                                for m_l, m_i in data["update_members"].items()}

                    for member_login, member_info in data["update_members"].items():
                        old_group = conf["members"].get(member_login, {}).get("group", "quest")
                        new_group = member_info.get("group", "quest")

                        if member_login not in conf["members"].keys():
                            access_error.append("Такой участник не существует")
                            break

                        if my_group in ("moderator", "member", "quest"):
                            if old_group in ("admin", "moderator"):
                                access_error.append("Группе \"%s\" запрещено изменять участников из группы \"%s\""
                                                    .format(my_group, old_group))
                                break
                            if new_group in ("admin", "moderator"):
                                access_error.append("Группе \"%s\" запрещено назначать группу \"%s\""
                                                    .format(my_group, old_group))
                                break

                        if my_group in ("member", "quest"):
                            access_error.append("Группе \"%s\" запрещено изменять пользователей"
                                                .format(my_group, old_group))
                            break

                    new_members_list = helpers.recursive_merge(conf["members"], data["update_members"])

                if len(access_error):
                    self.write_info(data,
                        status="405 Not Allowed",
                        info="\n".join(access_error)
                    )
                    return

                # Обновляем данные конференции.
                try:
                    yield database.setConference({"members": new_members_list, "login": data["login"]})
                except (database.Warning, database.Error):
                    logger.error("Error updating conference", exc_info=True)
                    self.write_info(data,
                        status="500 Internal Server Error",
                        info="Не удалось обновить список участников конференции"
                    )
                    return
            else:
                # другие skaro серверы
                conf = {}
                pass
        else:
            # создание конференции в других протоколах
            conf = {}
            pass


        # Извещаем пользователей о изменении конференции
        updates = {
            "type": "changed_conference_members",
            "login": data["login"]
        }

        if len(data.get("add_members", {})):
            # Добавляем новым участникам конференции конференцию в контакты
            try:
                yield database.addContactUsersByLogins(data["add_members"].keys(), {
                    data["login"]: {
                        "type": "conference"
                    }
                })
            except (database.Warning, database.Error):
                logger.error("Error adding conference in contacts new members", exc_info=True)
                self.write_client({
                    "type": "error",
                    "status": "500 Internal Server Error",
                    "info": "Не удалось добавить конференцию в контакты её новым участникам"
                })
                # todo: тут нужно удалять участников, которым не удалось добавить конфу в контакты
            else:
                self.send_updates({
                    "type": "changed_contacts",
                    "add_contacts": {data["login"]: {"type":"conference"}}
                }, users_for_update=data["add_members"].keys(), send_self_devices=True)

            updates["add_members"] = data["add_members"]

        if len(data.get("del_members", {})):
            # Удаляем перечисленных участников конференции
            try:
                yield database.addContactUsersByLogins(data["del_members"].keys(), {
                    data["login"]: None
                })
            except (database.Warning, database.Error):
                logger.error("Error deleting conference in contacts members", exc_info=True)
                self.write_client({
                    "type": "error",
                    "status": "500 Internal Server Error",
                    "info": "Не удалось удалить конференцию из контактов удаляемых участников"
                })
                # todo: тут нужно добавлять участников, которым не удалось удалить конфу из контактов
            else:
                self.send_updates({
                    "type": "changed_contacts",
                    # todo: set reason from data["del_members"][i]["reason"]
                    "del_contacts": {data["login"]: {"reason":"Some reason"}}
                }, users_for_update=data["del_members"].keys(), send_self_devices=True)

            updates["del_members"] = data["del_members"]

        if len(data.get("update_members", {})):
            updates["update_members"] = data["update_members"]

        self.send_updates(updates, for_conference=data["login"])

        # Операция прошла успешно
        if helpers.is_request(data):
            self.write_client({
                "chain_uuid": data["chain_uuid"],
                "type": "request",
                "status": "200 OK",
                "struct_type": "change_conference_members"
            })
