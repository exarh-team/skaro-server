#!/usr/bin/env python
# Skaro is the open standard for instant messaging
# Copyright © 2017 Exarh Team
#
# This file is part of skaro-server.
#
# Skaro-server is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import yaml  # PyYAML
import logging

logger = logging.getLogger(__name__)  # named logging for database.py


def read_all():
    with open(conf_path) as json_file:
        try:
            json_data = yaml.load(json_file)
            if json_data is None:
                json_data = {}
        except yaml.YAMLError as exc:
            json_data = {}
    return json_data


def read(key):
    if key in config:
        return config[key]
    else:
        return


def write(key, data):
    config[key] = data
    f = open(conf_path, 'x')
    f.write(yaml.dump(config))


if __name__ != "__main__":
    path = os.path.expanduser('~')
    if os.name == 'nt':
        path += '/Application Data'
    else:
        path += '/.config'

    if not os.path.exists(path):
        os.mkdir(path)

    path += '/skarim-server'

    if not os.path.exists(path):
        os.mkdir(path)

    conf_path = path + '/default.yaml'

    if os.path.exists(conf_path):
        config = read_all()
    else:
        config = {}
        open(conf_path, 'tw', encoding='utf-8')
