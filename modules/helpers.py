#!/usr/bin/env python
# Skaro is the open standard for instant messaging
# Copyright © 2017 Exarh Team
#
# This file is part of skaro-server.
#
# Skaro-server is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import tornado.gen
from tornado.options import options
import httpagentparser
from random import randint
import uuid

from modules import config


def recursive_merge(d1, d2):
    """Обновляет первый словарь дополняя его вторым словарем рекурсивно"""
    d2 = d2.copy()
    for k, v in d1.items():
        if k in d2:
            # значения словарь?
            if all(isinstance(e, dict) for e in (v, d2[k])):
                d2[k] = recursive_merge(v, d2[k])
    d1.update(d2)
    return d1


def is_request(data):
    """Проверяет нужен ли сообщению клиента ответ сервера"""
    if "chain_uuid" in data.keys() and type(data['chain_uuid']) is str:
        return True
    else:
        return False


def is_exist_keys(dict_data, *keys):
    """Проверяет ключи в словаре на существование и пустоту"""
    for key in keys:
        try:
            v = dict_data[key]
            if not v:
                raise KeyError()
        except KeyError:
            return False
    return True


def filtering_user_data(data):
    """Очищает данные юзера от конфиденциальных данных, не копирует входящий обьект"""
    if 'public_key' in data.keys():
        del data['public_key']
    for login, settings in data['accounts'].items():
        if 'public_key' in settings.keys():
            del settings['public_key']

    return data


def filtering_conf_data(data):
    """Очищает данные о конференции от конфиденциальных данных, не копирует входящий обьект"""
    if 'password' in data.keys():
        del data['password']
    if 'members' in data.keys():
        del data['members']

    return data


def parse_login(login, and_parse_skaro_login=False, to_recover_with="@"):
    x = login.split('::', 1)

    if len(x) < 2:
        x = ["skaro", '/'.join(parse_skaro_login(x[0], to_recover_with))]

    if and_parse_skaro_login:
        x[1:] = parse_skaro_login(x[1], to_recover_with)
    else:
        x[0] = x[0].lower()

    return tuple(x)


def parse_skaro_login(login, to_recover_with="@"):
    x = login.split('/', 1)

    if len(x) < 2:
        x = [options.host, x[0]]

    if x[1] and x[1][0] not in ("~", "@", "+"):
        x[1] = to_recover_with + x[1]

    x[0] = x[0].lower()

    return tuple(x)


def recover_full_login(login, with_symbol="@"):
    return "::".join(parse_login(login, to_recover_with=with_symbol))


def is_skaro_login(full_login):
    (protocol, host, login) = parse_login(full_login, True, to_recover_with="@")

    if protocol == "skaro" and login[0] == "@":
        return True
    else:
        return False


def is_skaro_conf_login(full_login):
    (protocol, host, login) = parse_login(full_login, True, to_recover_with="~")

    if protocol == "skaro" and login[0] == "~":
        return True
    else:
        return False


def generate_device_id(self):
    postfix = str(randint(10**3, 10**4-1))
    if "User-Agent" in self.request.headers:
        return ' '.join(
            httpagentparser.simple_detect(
                self.request.headers["User-Agent"]
            )[:-1]
        ).replace(" ", "_") + "_" + postfix
    else:
        return postfix


def TaskWrapper(func):
    """tornado.gen.Task wrapper for shorter calls"""

    def wrap(*args, **kwargs):
        return tornado.gen.Task(func, *args, **kwargs)

    return wrap


def getUUID(self=None):
    """Returns the new value for the "request id" key"""
    return str(uuid.uuid5(uuid.NAMESPACE_OID, str(id(self))))


def parse_args(args):
    """Returns the arguments contained in the strings of the list"""
    remaining = []
    for i in range(0, len(args)):
        if args[i].startswith("-"):
            line = args[i].split(" ")
            for arg in line:
                remaining.append(arg)
        else:
            continue
    return remaining
