#!/usr/bin/env python
# Skaro is the open standard for instant messaging
# Copyright © 2017 Exarh Team
#
# This file is part of skaro-server.
#
# Skaro-server is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging

# for timing
from datetime import datetime, timedelta

# Tornado server
import tornado.escape
import tornado.util
import tornado.ioloop
import tornado.web
import tornado.gen
import tornado.websocket

# No standard python libraries
import cbor

# Local modules and packages
import protocols
from modules import config, database, helpers

# Mixins for Main classes
from mixins.users import UsersManagement
from mixins.messages import MessManagement
from mixins.conferences import ConfManagement
from mixins.requests import RequestsManagement

logger = logging.getLogger(__name__)  # named logging for server.py


class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            (r"/skaro_client/", WSHandlerClient),
            (r"/skaro_server/", WSHandlerServer),
        ]
        settings = dict(
            xsrf_cookies=False,
            autoload=True
        )

        # Подключаемся к бд
        database.connect()

        # Множество подключенных в данный момент пользователей(не авторизованных)
        # Каждый пользователь, это экземпляр класса WSHandler
        # todo: организовать очистку от соединений, которые не авторизовались в течение 10ти минут.
        # todo: с выводом уведомления о причине разрыва соединения
        self.SocketsPool = set()

        # Словарь соединений авторизованных пользователей
        # перекладываются сюда из self.SocketsPool после авторизации
        # {
        #     "@SomeName": {
        #         "connections": {
        #              "__идентификатор устройства__": object,
        #              "__идентификатор устройства__": object,
        #              ...
        #         },
        #         "data": {USER DATA},
        #         "accounts": {PROTOCOLS HANDLERS}
        #     },
        #     ...
        # }
        self.AuthorizedPool = dict()

        # Сессии пользователей, потерявших соединение
        self.SessionsRestore = list()

        tornado.web.Application.__init__(self, handlers, **settings)

    # Чистильщик, работающий асинхронно
    # Следит за тем, чтобы прерванные сессии не лежали в хранилище слишком долго
    def cleaner_sessions_restore(self):
        # Осуществляем проход по хранилищу сессий
        for session in self.SessionsRestore:
            if session['__time_of_dead__'] < datetime.now():
                self.SessionsRestore.remove(session)
        # Запускам чистильщик снова, если есть что чистить
        if len(self.SessionsRestore) > 0:
            tornado.ioloop.IOLoop.instance().call_later(5, self.cleaner_sessions_restore)

    #Добавляет потерянную сессию в хранилище быстрого восстановления сессии
    def add_session_in_store(self, session_id=None, device_id=None, login=None, lifetime=5):
        # Узнаем, остановлен ли чистильщик сессий
        # Если хранилище сессий пустое, то чистильщик сессий остановлен
        start_cleaner = False
        if len(self.SessionsRestore) < 1:
            start_cleaner = True

        # Временно сохраняем сессию в хранилище
        self.SessionsRestore.append({
            "session_id": session_id,
            "login": helpers.recover_full_login(login),
            "device_id": device_id,
            "__time_of_dead__": datetime.now() + timedelta(minutes=lifetime),
        })

        # Запускаем чистильщик сессий, если он был остановлен
        if start_cleaner:
            tornado.ioloop.IOLoop.instance().call_later(lifetime, self.cleaner_sessions_restore)


class WSHandler(RequestsManagement, tornado.websocket.WebSocketHandler):
    # Проверка подключения
    def check_origin(self, origin):
        logger.info(origin)
        return True

    # Если соединение открывается с указанием подпротокола
    # todo: можно использовать для поддержки не только CBOR, но и JSON и т.п.
    def select_subprotocol(self, subprotocols):
        if "cbor" in subprotocols:
            return "cbor"
        elif "cbor-typed" in subprotocols:
            return "cbor-typed"
        else:
            self.request.headers['Sec-Websocket-Protocol'] = "cbor"
            return None

    # Декодируем сообщение от клиента
    def read_client(self, message):
        try:
            data = cbor.loads(message)
        except Exception:
            logger.error("Error decoding message", exc_info=True)
            return False
        else:
            return data

    # Отправляем сообщение клиенту
    def write_client(self, message):
        if self.request.headers['Sec-Websocket-Protocol'] != "cbor-typed" and "struct_type" in message:
            del message['struct_type']

        logger.info(message)
        try:
            message = cbor.dumps(message)
            self.ws_connection.write_message(message, binary=True)
        except Exception:
            logger.error("Error sending message", exc_info=True)

    # Отправка сообщения с ошибкой или успехом операции
    def write_info(self, data, status="400 Bad Request", info="Some error"):
        if helpers.is_request(data):
            self.write_client({
                "chain_uuid": data["chain_uuid"],
                "type": "request",
                "status": status,
                "info": info,
                "struct_type": data["type"]
            })
        else:
            self.write_client({
                "type": "error",
                "status": status,
                "info": info,
            })

    # Генерация уникального session_id
    def generate_session_id(self):
        return helpers.getUUID(self)


class WSHandlerClient(UsersManagement, MessManagement, ConfManagement, WSHandler):

    # Первое подключение, открытие ws соединения
    def open(self):
        self.uid = id(self)
        self.device_id = helpers.generate_device_id(self)

        logger.info("Кто-то открыл соединение: " + self.device_id + "(" + str(self.uid) + ")")

    # Соединение прервалось
    def on_close(self):

        if self.is_auth():
            logger.info("Пользователь потерял соединение: " + self.session.data['login'] + "(" + str(self.device_id) + ")")
        else:
            logger.info("Кто-то потерял соединение: " + self.device_id + "(" + str(self.uid) + ")")

            # заканчиваем операции для не авторизованных
            del self
            return


        # Удаляем из онлайна
        del self.session.connections[self.device_id]
        if not len(self.session.connections):
            self.session.data["status"] = "offline"
            self.send_updates({
                "type": "changed_user_data",
                "login": self.session.data['login'],
                "update": {
                    "status":"offline"
                }
            })

            del self.application.AuthorizedPool[
                helpers.parse_skaro_login(self.session.data['login'])[1]
            ]

        # Добавляем сесссию потерянного соединения в хранилище быстрого восстановления сессий
        self.application.add_session_in_store(
            session_id=self.session_id,
            device_id=self.device_id,
            login=self.session.data['login']
        )

        del self

    # Перехватываем сообщение
    def on_message(self, message):
        if self.is_auth():
            logger.info("От пользователя " + self.session.data['login'] + "(" + str(self.device_id) + ")")
        else:
            logger.info("От неавторизованного пользователя " + self.device_id + "(" + str(self.uid) + ")")

        # Декодируем данные
        data = self.read_client(message)
        logger.info("Получено сообщение %r", data)

        # Проверка формата данных
        if not data:
            self.write_client({
                "type": "error",
                "status": "400 Bad Request",
                "info": "Неверный формат, не удалось декодировать сообщение",
            })

            return
        elif type(data) is not dict:
            self.write_client({
                "type": "error",
                "status": "400 Bad Request",
                "info": "Неверный формат сообщения: нарушена структура \"ключ-значение\"",
            })

            return
        elif "type" not in data.keys() or type(data['type']) is not str:
            if helpers.is_request(data):
                self.write_client({
                    "chain_uuid": data["chain_uuid"],
                    "type": "request",
                    "status": "400 Bad Request",
                    "info": "Неверный формат сообщения: отсутствует поле \"type\"",
                    "struct_type": "unknown"
                })
            else:
                self.write_client({
                    "type": "error",
                    "status": "400 Bad Request",
                    "info": "Неверный формат сообщения: отсутствует поле \"type\"",
                })

            return

        # Запросы, на которые сервер должен отвечать всегда
        # Запрос на авторизацию, тип регистрации, регистрацию, восстановление сессии подключения
        if data["type"] in ("authorize", "register_type",
                            "register", "session_restore", "request"):
            try:
                getattr(self, "event_" + data["type"])(data)
            except Exception:
                logger.error("Error in handler of type: %s", data["type"], exc_info=True)

            return

        # Остальные виды запросов только после авторизации
        # Если гость, то выводим соответствующее сообщение
        elif not self.is_auth():
            if helpers.is_request(data):
                self.write_client({
                    "chain_uuid": data["chain_uuid"],
                    "type": "request",
                    "status": "401 Unauthorized",
                    "info": "Данный тип сообщений поддерживается только после авторизации или не поддерживается вообще.",
                    "struct_type": data["type"]
                })
            else:
                self.write_client({
                    "type": "error",
                    "status": "401 Unauthorized",
                    "info": "Данный тип сообщений (%s) поддерживается только после авторизации или не поддерживается вообще."
                            % (data["type"],),
                })

            return

        # Вызов обработчиков остальных сообщений
        try:
            getattr(self, "event_" + data["type"])(data)
        except AttributeError:
            if helpers.is_request(data):
                self.write_client({
                    "chain_uuid": data["chain_uuid"],
                    "type": "request",
                    "status": "400 Bad Request",
                    "info": "Данный тип сообщений не поддерживается.",
                    "struct_type": data["type"]
                })
            else:
                self.write_client({
                    "type": "error",
                    "status": "400 Bad Request",
                    "info": "Данный тип сообщений ('" + data["type"] + "') не поддерживается.",
                })
        except Exception:
            logger.error("Error in handler of type: %s", data["type"], exc_info=True)


class WSHandlerServer(WSHandler):
    def on_message(self, message):
        data = self.read_client(message)
        logger.info("got message from server %r", data)
