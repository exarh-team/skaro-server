#!/usr/bin/env python
# Skaro is the open standard for instant messaging
# Copyright © 2017 Exarh Team
#
# This file is part of skaro-server.
#
# Skaro-server is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#
# Validate module
# validating strings and e.t.c.
#
#

import logging
from re import error as ReException, compile, match, ASCII, DOTALL, IGNORECASE, LOCALE, MULTILINE, UNICODE, VERBOSE
from datetime import datetime
from functools import wraps

P_LOGIN = compile(r"^[\w\-]+$", flags=IGNORECASE | UNICODE)
P_MAIL = compile(r"^.+@.+\..+$", flags=IGNORECASE | UNICODE)
P_DEVICE_ID = compile(r"^[\w\-\_\.]+$", flags=IGNORECASE | UNICODE)
P_STATUS = compile(r"^[\d]{3}(\s[\w\-\,\.\:]+)*$", flags=IGNORECASE | UNICODE)

logger = logging.getLogger(__name__)  # named logging for validate.py


def is_valid(value, pattern):
    try:
        if match(pattern, value):
            return True
    except ReException:
        logger.error("Error in re.math(%s, %s)", str(pattern), str(value), exc_info=True)

    return False


def arguments(*args, error_handler=None, use_accurate_informing=True, **kwargs):

    as_decorator = len(args) == 1 and len(kwargs) == 0 and callable(args[0])

    def decorator(f):
        @wraps(f)
        def wrapper(self, *af, **kwf):
            # af и kwf - аргументы декорируемой функции f
            # args и kwargs - дополнительные параметры декоратора

            pos = 0
            for name, checker in f.__annotations__.items():

                if name in kwf:
                    e = checker.check_data(kwf.get(name))
                else:
                    try:
                        arg = af[pos]
                        pos += 1
                    except IndexError:
                        arg = None
                    finally:
                        e = checker.check_data(arg)

                if e:
                    data = {}
                    if use_accurate_informing:
                        data = af[0]

                    if error_handler is None:
                        self.write_info(data, info=e)
                    else:
                        error_handler(data, info=e)
                    return

            return f(self, *af, **kwf)
        return wrapper

    if as_decorator:
        # Если первый позиционный аргумент - функция, то работаем как декоратор
        return decorator(args[0])
    else:
        # Иначе, конфигурируем декоратор, и возвращаем его обьект-функцию
        return decorator


class DictSchema(object):
    """
    Create schema
    DictSchema(
        # Special flags
        DictSchema.ALLOW_MORE_KEYS
        ...
        # Keys for validation
        key_name = StringValue(required=True,min=10,max=20....),
        key_name_2 = ListValue(min=1, item=DictSchema(...),
        ...
    )
    for check "data" by using check_data(data) method
    that returned error string or False if "data" are valid

    """

    ALLOW_MORE_KEYS = -10

    def __init__(self, *flags, **fields):
        self.flags = flags
        self.fields = fields

    def check_data(self, data):
        if data is None:
            return " Значение должно быть указано"
        if type(data) is not dict:
            return " Значение должно быть ассоциативным массивом"
        if self.fields:
            # Проверка на лишние ключи
            if self.ALLOW_MORE_KEYS not in self.flags:
                errors = []
                for key in data.keys():
                    if key not in self.fields.keys():
                        errors.append(("['%s']" % (key,)) + " Лишний ключ")

                if errors:
                    return errors
            # Проверка ключей
            errors = []
            for key, check_obj in self.fields.items():
                s = check_obj.check_data(data.get(key))
                if s:
                    errors.append(("['%s']" % (key,)) + s)
            if errors:
                return errors

    def __repr__(self):
        return "DictSchema(" + ", ".join("%s = %s" % param for param in self.fields.items()) + ")"


class IntValue(object):
    def __init__(self, required=False, min=None, max=None):
        self.required = required
        self.min = min
        self.max = max

    def check_data(self, data):
        if data is not None and type(data) is not int:
            return " Значение должно быть целым числом"
        if not data and self.required:
            return " Значение должно быть заполнено"
        if data is not None:
            if self.min is not None and data < self.min:
                return " Значение не должно быть меньше числа %d" % (self.min,)
            if self.max is not None and data > self.max:
                return " Значение не должно быть больше числа %d" % (self.max,)

    def __repr__(self):
        return "IntValue(" \
               + ", ".join("%s = %s" % param for param in
                           (("required", self.required),
                            ("min", self.min),
                            ("max", self.max))) \
               + ")"


class NumericValue(object):
    def __init__(self, required=False, min=None, max=None):
        self.required = required
        self.min = min
        self.max = max

    def check_data(self, data):
        if data is not None \
                and type(data) is not int and type(data) is not float:
            return " Значение должно быть числом"
        if not data and self.required:
            return " Значение должно быть заполнено"
        if data is not None:
            if self.min is not None and data < self.min:
                return " Значение не должно быть меньше длины %d" % (self.min,)
            if self.max is not None and data > self.max:
                return " Значение не должно быть больше длины %d" % (self.max,)

    def __repr__(self):
        return "NumericValue(" \
               + ", ".join("%s = %s" % param for param in
                           (("required", self.required),
                            ("min", self.min),
                            ("max", self.max))) \
               + ")"


class BoolValue(object):
    def __init__(self, required=False):
        self.required = required

    def check_data(self, data):
        if data is not None and type(data) is not bool:
            return " Значение должно быть либо True либо False"
        if not data and self.required:
            return " Значение должно быть заполнено"

    def __repr__(self):
        return "BoolValue(" + "%s = %s" % ("required", self.required) + ")"


class StringValue(object):
    def __init__(self, required=False, min=None, max=None, pattern=None, prepare_callback=None):
        self.required = required
        self.pattern = pattern
        self.prepare_callback = prepare_callback
        self.min = min
        self.max = max

    def check_data(self, data):
        if data is not None and type(data) is not str:
            return " Значение должно быть строкой"
        if not data and self.required:
            return " Значение должно быть заполнено"

        if data and self.prepare_callback is not None:
            data = self.prepare_callback(data)
        
        if data is not None:
            if self.min is not None and len(data) < self.min:
                return " Значение не должно быть меньше длины %d" % (self.min,)
            if self.max is not None and len(data) > self.max:
                return " Значение не должно быть больше длины %d" % (self.max,)
            if data and self.pattern is not None and not is_valid(data, self.pattern):
                return " Значение не корректно"

    def __repr__(self):
        return "StringValue(" \
               + ", ".join("%s = %s" % param for param in
                           (("required", self.required),
                            ("min", self.min),
                            ("max", self.max),
                            ("pattern", self.pattern))) \
               + ")"


class ListValue(object):
    def __init__(self, required=False, item=None, min=None, max=None):
        self.required = required
        self.item = item
        self.min = min
        self.max = max

    def check_data(self, data):
        if data is not None and type(data) is not list:
            return " Значение должно быть списком(массивом)"
        if not data and self.required:
            return " Значение должно быть заполнено"

        if data is not None:
            if self.min is not None and len(data) < self.min:
                return " Количество элементов не должно быть меньше длины %d" % (self.min,)
            if self.max is not None and len(data) > self.max:
                return " Количество элементов не должно быть больше длины %d" % (self.max,)
            if self.item is not None:
                for i, item in enumerate(data):
                    s = self.item.check_data(item)
                    if s:
                        return ("[%d]" % (i,)) + s

    def __repr__(self):
        return "ListValue(" \
               + ", ".join("%s = %s" % param for param in
                           (("required", self.required),
                            ("item", self.item),
                            ("min", self.min),
                            ("max", self.max))) \
               + ")"


class DictValue(object):
    def __init__(self, required=False, keys=None, values=None, min=None, max=None):
        self.required = required
        self.keys = keys
        self.values = values
        self.min = min
        self.max = max

    def check_data(self, data):
        if data is not None and type(data) is not dict:
            return " Значение должно быть списком(массивом)"
        if not data and self.required:
            return " Значение должно быть заполнено"

        if data is not None:
            if self.min is not None and len(data) < self.min:
                return " Количество элементов не должно быть меньше длины %d" % (self.min,)
            if self.max is not None and len(data) > self.max:
                return " Количество элементов не должно быть больше длины %d" % (self.max,)
            if self.keys is not None or self.values is not None:
                for key, val in data.items():
                    if self.keys is not None:
                        s = self.keys.check_data(key)
                        if s:
                            return ("[%s]" % (key,)) + s

                    if self.values is not None:
                        s = self.values.check_data(val)
                        if s:
                            return ("[%s]" % (key,)) + s

    def __repr__(self):
        return "DictValue(" \
               + ", ".join("%s = %s" % param for param in
                           (("required", self.required),
                            ("keys", self.keys),
                            ("values", self.values),
                            ("min", self.min),
                            ("max", self.max))) \
               + ")"


class DatetimeValue(object):
    def __init__(self, required=False, min=None, max=None):
        self.required = required
        self.min = min
        self.max = max

    def check_data(self, data):
        if data is not None and type(data) is not datetime:
            return " Значение должно быть валидной временной меткой"
        if not data and self.required:
            return " Значение должно быть заполнено"

        if data is not None:
            if self.min is not None and data < self.min:
                return " Значение не должно быть меньше даты %s" % (self.min,)
            if self.max is not None and data > self.max:
                return " Значение не должно быть больше даты %s" % (self.max,)

    def __repr__(self):
        return "NumericValue(" \
               + ", ".join("%s = %s" % param for param in
                           (("required", self.required),
                            ("min", self.min),
                            ("max", self.max))) \
               + ")"
