Требуются Python3, pip и PostgreSQL(>= 9.4.1)

# Install and start

## Installing prerequisites

Installing prerequisites on Ubuntu or Debian:

`sudo apt install build-essential python3-dev python3-pip python3-virtualenv`

## Creating / Initiating a virtual environment

To install the skaro server run:

```
virtualenv -p python3 .
source bin/activate
```

If this is the first start of the virtual environment, run the command:

`pip install -r requirements.txt`


***Remember**: When you are done working with one environment, before switching to another - or working with the globally installed one - you need to make sure to deactivate it.*

## Start

To start your skaro server, run (in your virtualenv):

`python __main__.py`

The list of possible arguments can be found with the command:

`python __main__.py --help`


# One more thing...

По адресу https://gitlab.com/exarh-team/docker-skaro/blob/master/etc/nginx/sites-available/default
смотрите пример настройки проксирования Tornado server(только WebSocket) через nginx для запуска веб-клиента https://gitlab.com/exarh-team/Skaro-WEB

В файле postgres-setup.sql находится дамп базы данных PostgreSQL. Необходимо импортировать этот .sql файл в вашу PostgreSQL, чтобы начать работу.

Обратите внимание клиентская часть настроена на подключение к WebSocket по адресу yourhost/skaro_server/ и yourhost/skaro_client/

---

А о том, как запустить skaro сервер на своей машине за 3 команды с помощью Docker, смотрите здесь:
https://gitlab.com/exarh-team/docker-skaro
