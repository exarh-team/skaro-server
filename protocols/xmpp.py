#!/usr/bin/env python
# Skaro is the open standard for instant messaging
# Copyright © 2017 Exarh Team
#
# This file is part of skaro-server.
#
# Skaro-server is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class common:
    def __init__(self, WS, settings):
        self.WS = WS

    def get_user_data(self, login_in_prl):
        return False

    def get_contacts_settings(self):
        return {}

    def get_contacts(self):
        return []

    def send_message(self, to, body):
        return False

    def send_conf_message(self, to, body):
        return False

    def show_message(self, xmpp_event):
        pass