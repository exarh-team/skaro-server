#!/usr/bin/env python
# Skaro is the open standard for instant messaging
# Copyright © 2017 Exarh Team
#
# This file is part of skaro-server.
#
# Skaro-server is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from tornado import testing, gen
import logging
import sys
from tornado.options import parse_command_line

from modules import helpers

logger = logging.getLogger(__name__)

# add configuration of server from command line(stored in options)
parse_command_line(helpers.parse_args(sys.argv))


class TestMessages:

    @gen.coroutine
    def _test_message_send_message_1(self):
        self.write_server({
            'type': "send_message",
            'chain_uuid': "45",
            'to': "@test-another-user",
            'body': """Все люди рождаются свободными и равными в своих достоинствах и правах.
Они наделены разумом и совестью и должны поступать в отношении друг друга в духе братства.
Каждый человек имеет право на жизнь, свободу и на личную неприкосновенность.
Никто не должен подвергаться пыткам или жестоким и унижающим его достоинство обращению и наказанию.
Все люди равны перед законом и имеют право, без всякого различия, на равную защиту закона.
Каждый человек имеет право на свободу мысли, совести и религии, на свободу мирных собраний и ассоциаций, на свободу убеждений и на свободное выражение их."""
        })

        response = yield self.read_request(lambda x: x["type"] == "request" and x["chain_uuid"] == "45")
        self.assertEqual(len(response['date']), 19)
        del response['date']
        self.assertEqual(response, {
            'chain_uuid': "45",
            'type': 'request',
            'status': '200 OK',
            'mid': 1
        })

    @gen.coroutine
    def _test_message_get_history_1(self):
        self.write_server({
            'type': "get_history",
            'chain_uuid': "77",
            'login': "@test-another-user"
        })

        response = yield self.read_request(lambda x: x["type"] == "request" and x["chain_uuid"] == "77")
        self.assertEqual(len(response['history'][0]['date']), 19)
        del response['history'][0]['date']
        del response['history'][0]['from']
        self.assertEqual(response, {
            'chain_uuid': "77",
            'type': 'request',
            'history': [
                {
                    'mid': 1,
                    'body': """Все люди рождаются свободными и равными в своих достоинствах и правах.
Они наделены разумом и совестью и должны поступать в отношении друг друга в духе братства.
Каждый человек имеет право на жизнь, свободу и на личную неприкосновенность.
Никто не должен подвергаться пыткам или жестоким и унижающим его достоинство обращению и наказанию.
Все люди равны перед законом и имеют право, без всякого различия, на равную защиту закона.
Каждый человек имеет право на свободу мысли, совести и религии, на свободу мирных собраний и ассоциаций, на свободу убеждений и на свободное выражение их.""",
                    'removed': []
                }
            ],
            'status': '200 OK'
        })

    @gen.coroutine
    def _test_message_clear_history_1(self):
        self.write_server({
            'type': "clear_history",
            'chain_uuid': "24",
            'login': "@test-another-user"
        })

        # response = yield self.read_request(lambda x: x["type"] == "request" and x["chain_uuid"] == "24")
        # print(response)
        # self.assertEqual(response, {
        #     'chain_uuid': "24",
        #     'status': '200 OK',
        #     'type': 'request'
        # })


if __name__ == '__main__':
    parse_command_line()
    testing.main()
