#!/usr/bin/env python
# Skaro is the open standard for instant messaging
# Copyright © 2017 Exarh Team
#
# This file is part of skaro-server.
#
# Skaro-server is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Testing modules.helpers
"""

from tornado.options import define, options
import unittest
from modules import helpers, config

define("host", default=config.read("host"), type=str,
       help="domain of the site running the server (without http:// or https://)")


class HelpersTestCase(unittest.TestCase):

    def test_recursive_merge(self):
        test_data_1 = {
            'login': "login",
            'password': "password",
            'accounts': {
                'login1': {
                    'full_name': "John Smith",
                    'password': "password"
                }
            }
        }
        test_data_2 = {
            'login': "John Smith",
            'accounts': {
                'login2': {
                    'full_name': "John Smith"
                }
            }
        }
        test_data_merge = {
            'login': "John Smith",
            'password': "password",
            'accounts': {
                'login1': {
                    'full_name': "John Smith",
                    'password': "password"
                },
                'login2': {
                    'full_name': "John Smith"
                }
            }
        }
        self.assertEqual(
            helpers.recursive_merge(test_data_1, test_data_2),
            test_data_merge
        )

    def test_is_request(self):
        test_data_1 = {
            'login': "login",
            'password': "password"
        }
        test_data_2 = {
            'login': "login",
            'password': "password",
            'chain_uuid': "42"
        }
        test_data_3 = {
            'login': "login",
            'password': "password",
            'chain_uuid': "42"
        }
        self.assertFalse(
            helpers.is_request(test_data_1)
        )
        self.assertTrue(
            helpers.is_request(test_data_2)
        )
        self.assertTrue(
            helpers.is_request(test_data_3)
        )

    def test_is_exist_keys(self):
        test_data_1 = {
            'login': "login",
            'password': "password"
        }
        self.assertTrue(
            helpers.is_exist_keys(test_data_1, "login")
        )
        self.assertFalse(
            helpers.is_exist_keys(test_data_1, "qwerty")
        )

    def test_filtering_user_data(self):
        test_data_1 = {
            'login': "login",
            'public_key': "public_key",
            'accounts': {
                'login': {
                    'full_name': "John Smith",
                    'public_key': "public_key"
                }
            }
        }
        test_clean_data = {
            'login': "login",
            'accounts': {
                'login': {
                    'full_name': "John Smith"
                }
            }
        }
        self.assertEqual(
            helpers.filtering_user_data(test_data_1.copy()),
            test_clean_data
        )

    def test_filtering_conf_data(self):
        test_data_1 = {
            'login': "login",
            'password': "password"
        }
        test_data_2 = {
            'login': "login",
            'members': "members"
        }
        test_clean_data = {
            'login': "login"
        }
        self.assertEqual(
            helpers.filtering_conf_data(test_data_1.copy()),
            test_clean_data
        )
        self.assertEqual(
            helpers.filtering_conf_data(test_data_2.copy()),
            test_clean_data
        )

    def test_parse_login(self):
        # Default testing
        test_login = "%s/@SomeName" % (options.host,)
        self.assertEqual(
            helpers.parse_login("SomeName"),
            ("skaro", test_login)
        )
        self.assertEqual(
            helpers.parse_login("skaro::SomeName"),
            ("skaro", "SomeName")
        )
        self.assertEqual(
            helpers.parse_login("skaro::@SomeName"),
            ("skaro", "@SomeName")
        )
        self.assertEqual(
            helpers.parse_login("skaro::"+test_login),
            ("skaro", test_login)
        )

        # testing with skaro login
        self.assertEqual(
            helpers.parse_login("SomeName", and_parse_skaro_login=True),
            ("skaro", options.host, "@SomeName")
        )
        self.assertEqual(
            helpers.parse_login("skaro::SomeName", and_parse_skaro_login=True),
            ("skaro", options.host, "@SomeName")
        )
        self.assertEqual(
            helpers.parse_login("skaro::@SomeName", and_parse_skaro_login=True),
            ("skaro", options.host, "@SomeName")
        )
        self.assertEqual(
            helpers.parse_login("skaro::"+test_login, and_parse_skaro_login=True),
            ("skaro", options.host, "@SomeName")
        )

    def test_parse_skaro_login(self):
        test_login = "%s/@SomeName" % (options.host,)
        self.assertEqual(
            helpers.parse_skaro_login(test_login),
            (options.host, "@SomeName")
        )
        self.assertEqual(
            helpers.parse_skaro_login("@SomeName"),
            (options.host, "@SomeName")
        )
        self.assertEqual(
            helpers.parse_skaro_login("SomeName"),
            (options.host, "@SomeName")
        )

    def test_recover_full_login(self):
        test_login = "skaro::%s/@SomeName" % (options.host,)
        self.assertEqual(
            helpers.recover_full_login("skaro::%s/@SomeName" % (options.host,)),
            test_login
        )
        self.assertEqual(
            helpers.recover_full_login("%s/@SomeName" % (options.host,)),
            test_login
        )
        self.assertEqual(
            helpers.recover_full_login("@SomeName"),
            test_login
        )
        self.assertEqual(
            helpers.recover_full_login("SomeName"),
            test_login
        )

    def test_is_skaro_login(self):
        self.assertTrue(
            helpers.is_skaro_login("skaro::%s/@SomeName" % (options.host,))
        )
        self.assertFalse(
            helpers.is_skaro_login("xmpp::%s/@SomeName" % (options.host,))
        )
        self.assertTrue(
            helpers.is_skaro_login("%s/@SomeName" % (options.host,))
        )
        self.assertTrue(
            helpers.is_skaro_login("@SomeName")
        )
        self.assertTrue(
            helpers.is_skaro_login("SomeName")
        )

    def test_is_skaro_conf_login(self):
        self.assertTrue(
            helpers.is_skaro_conf_login("skaro::%s/~SomeName" % (options.host,))
        )
        self.assertFalse(
            helpers.is_skaro_conf_login("xmpp::%s/~SomeName" % (options.host,))
        )
        self.assertTrue(
            helpers.is_skaro_conf_login("%s/~SomeName" % (options.host,))
        )
        self.assertTrue(
            helpers.is_skaro_conf_login("~SomeName")
        )
        self.assertTrue(
            helpers.is_skaro_conf_login("SomeName")
        )


if __name__ == '__main__':
    unittest.main()
