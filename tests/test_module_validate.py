#!/usr/bin/env python
# Skaro is the open standard for instant messaging
# Copyright © 2017 Exarh Team
#
# This file is part of skaro-server.
#
# Skaro-server is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import unittest
from modules import validate
from datetime import datetime


class ValidateTestCase(unittest.TestCase):

    def test_is_valid(self):
        self.assertTrue(
            validate.is_valid("login", validate.P_LOGIN)
        )
        self.assertTrue(
            validate.is_valid("мой-логин", validate.P_LOGIN)
        )
        self.assertFalse(
            validate.is_valid("login'", validate.P_LOGIN)
        )

        self.assertTrue(
            validate.is_valid("simplewithsymbol@example.com", validate.P_MAIL)
        )
        self.assertTrue(
            validate.is_valid("less.common@example.com", validate.P_MAIL)
        )
        self.assertTrue(
            validate.is_valid("a.little.more.unusual@dept.example.com", validate.P_MAIL)
        )
        self.assertTrue(
            validate.is_valid("!#$%&'*+-/=?^_`{}|~@example.org", validate.P_MAIL)
        )
        self.assertFalse(
            validate.is_valid("mail", validate.P_MAIL)
        )

        self.assertTrue(
            validate.is_valid("linux-4.9", validate.P_DEVICE_ID)
        )
        self.assertFalse(
            validate.is_valid("windows@localhost", validate.P_DEVICE_ID)
        )

        self.assertTrue(
            validate.is_valid("200 OK", validate.P_STATUS)
        )
        self.assertTrue(
            validate.is_valid("200", validate.P_STATUS)
        )
        self.assertFalse(
            validate.is_valid("Not found", validate.P_STATUS)
        )

    def test_arguments(self):

        #
        # test 1
        #

        def test_exception_1(a, info=False):
            self.assertEqual(
                info,
                " Значение должно быть указано"
            )

        @validate.arguments(error_handler=test_exception_1)
        def test_errors_1(parent, data: validate.DictSchema(
            type=validate.StringValue()
        )):
            self.fail(data)

        test_errors_1(self, None)

        #
        # test 2
        #

        def test_exception_2(a, info=False):
            self.assertEqual(
                info,
                " Значение должно быть ассоциативным массивом"
            )

        @validate.arguments(error_handler=test_exception_2)
        def test_errors_2(parent, data: validate.DictSchema(
            type=validate.StringValue()
        )):
            self.fail(data)

        test_errors_2(self, [])

        #
        # test 3
        #

        def test_exception_3(a, info=False):
            self.assertEqual(
                info,
                ["['chain_uuid'] Лишний ключ"]
            )

        @validate.arguments(error_handler=test_exception_3)
        def test_errors_3(parent, data: validate.DictSchema(
            type=validate.StringValue()
        )):
            self.fail(data)

        test_errors_3(self, {
            'type': 'register_type',
            'chain_uuid': "9"
        })

        #
        # test 4
        #

        def test_exception_4(a, info=False):
            self.assertCountEqual(
                info,
                ["['wrong_string'] Значение должно быть строкой",
                 "['wrong_list'] Значение должно быть списком(массивом)",
                 "['wrong_int'] Значение должно быть целым числом",
                 "['wrong_numeric'] Значение должно быть числом",
                 "['wrong_bool'] Значение должно быть либо True либо False",
                 "['wrong_date'] Значение должно быть валидной временной меткой",
                 "['wrong_dict'] Значение должно быть списком(массивом)"]
            )

        @validate.arguments(error_handler=test_exception_4)
        def test_errors_4(parent, data: validate.DictSchema(
            wrong_int=validate.IntValue(),
            wrong_numeric=validate.NumericValue(),
            wrong_bool=validate.BoolValue(),
            wrong_string=validate.StringValue(),
            wrong_list=validate.ListValue(),
            wrong_dict=validate.DictValue(),
            wrong_date=validate.DatetimeValue()
        )):
            self.fail(data)

        test_errors_4(self, {
            'wrong_int': 1.1,
            'wrong_numeric': 'num',
            'wrong_bool': 11,
            'wrong_string': 11,
            'wrong_list': 11,
            'wrong_dict': 11,
            'wrong_date': 11
        })

        #
        # test 5
        #

        def test_exception_5(a, info=False):
            self.assertCountEqual(
                info,
                ["['wrong_date'] Значение должно быть заполнено",
                 "['wrong_numeric'] Значение должно быть заполнено",
                 "['wrong_dict'] Значение должно быть заполнено",
                 "['wrong_int'] Значение должно быть заполнено",
                 "['wrong_bool'] Значение должно быть заполнено",
                 "['wrong_string'] Значение должно быть заполнено",
                 "['wrong_list'] Значение должно быть заполнено"]
            )

        @validate.arguments(error_handler=test_exception_5)
        def test_errors_5(parent, data: validate.DictSchema(
            wrong_int=validate.IntValue(required=True),
            wrong_numeric=validate.NumericValue(required=True),
            wrong_bool=validate.BoolValue(required=True),
            wrong_string=validate.StringValue(required=True),
            wrong_list=validate.ListValue(required=True),
            wrong_dict=validate.DictValue(required=True),
            wrong_date=validate.DatetimeValue(required=True)
        )):
            self.fail(data)

        test_errors_5(self, {})

        #
        # test 6
        #

        def test_exception_6(a, info=False):
            self.fail(info)

        @validate.arguments(error_handler=test_exception_6)
        def test_errors_6(parent, data: validate.DictSchema(
            type=validate.StringValue(),
            chain_uuid=validate.StringValue(),
            login=validate.StringValue(
                required=True,
                max=255,
                pattern=validate.P_LOGIN
            ),
            number=validate.NumericValue(),
            is_true=validate.BoolValue(),
            hex_alphabet=validate.ListValue(),
            list_of_lists=validate.ListValue(
                item=validate.ListValue()
            ),
            val_dict=validate.DictValue(),
            date_now=validate.DatetimeValue()
        )):
            self.assertEqual(
                data,
                {
                    'type': 'register_type',
                    'chain_uuid': "9",
                    'login': 'user',
                    'number': 4815,
                    'is_true': True,
                    'hex_alphabet': ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'],
                    'list_of_lists': [['a'], ['b'], ['c']],
                    'val_dict': {'key1': 1, 'key2': 2},
                    'date_now': datetime(2004, 9, 22, 13, 0, 38, 119173)
                }
            )

        test_errors_6(self, {
            'type': 'register_type',
            'chain_uuid': "9",
            'login': "user",
            'number': 4815,
            'is_true': True,
            'hex_alphabet': ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'],
            'list_of_lists': [['a'], ['b'], ['c']],
            'val_dict': {'key1': 1, 'key2': 2},
            'date_now': datetime(2004, 9, 22, 13, 0, 38, 119173)
        })

        #
        # test int
        #

        def test_exception_int(a, info=False):
            self.assertCountEqual(
                info,
                ["['int_small'] Значение не должно быть меньше числа 10",
                 "['int_big'] Значение не должно быть больше числа 100"]
            )

        @validate.arguments(error_handler=test_exception_int)
        def test_errors_int(parent, data: validate.DictSchema(
            int_big=validate.IntValue(
                max=100
            ),
            int_small=validate.IntValue(
                min=10
            )
        )):
            self.fail(data)

        test_errors_int(self, {
            'int_big': 999,
            'int_small': 0
        })

        #
        # test numeric
        #

        def test_exception_numeric(a, info=False):
            self.assertCountEqual(
                info,
                ["['numeric_small'] Значение не должно быть меньше длины 10",
                 "['numeric_big'] Значение не должно быть больше длины 100"]
            )

        @validate.arguments(error_handler=test_exception_numeric)
        def test_errors_numeric(parent, data: validate.DictSchema(
            numeric_big=validate.NumericValue(
                max=100
            ),
            numeric_small=validate.NumericValue(
                min=10
            )
        )):
            self.fail(data)

        test_errors_numeric(self, {
            'numeric_big': 999,
            'numeric_small': 0.6
        })

        #
        # test string
        #

        def test_exception_string(a, info=False):
            self.assertCountEqual(
                info,
                ["['string_small'] Значение не должно быть меньше длины 10",
                 "['string_big'] Значение не должно быть больше длины 10", "['string_pattern'] Значение не корректно"]
            )

        @validate.arguments(error_handler=test_exception_string)
        def test_errors_string(parent, data: validate.DictSchema(
            string_big=validate.StringValue(
                max=10
            ),
            string_small=validate.StringValue(
                min=10
            ),
            string_pattern=validate.StringValue(
                pattern=validate.P_LOGIN
            )
        )):
            self.fail(data)

        test_errors_string(self, {
            'string_big': "long long string",
            'string_small': "",
            'string_pattern': "is not a login"
        })

        #
        # test list
        #

        def test_exception_list(a, info=False):
            self.assertCountEqual(
                info,
                ["['list_of_lists'][0] Значение должно быть строкой",
                 "['list_big'] Количество элементов не должно быть больше длины 2",
                 "['list_small'] Количество элементов не должно быть меньше длины 1"]
            )

        @validate.arguments(error_handler=test_exception_list)
        def test_errors_list(parent, data: validate.DictSchema(
            list_big=validate.ListValue(
                max=2
            ),
            list_small=validate.ListValue(
                min=1
            ),
            list_of_lists=validate.ListValue(
                item=validate.StringValue()
            ),
        )):
            self.fail(data)

        test_errors_list(self, {
            'list_big': [1, 2, 3, 4],
            'list_small': [],
            'list_of_lists': [[1], [2]]
        })

        #
        # test dict
        #

        def test_exception_dict(a, info=False):
            self.assertCountEqual(
                info,
                ["['dict_values'][key1] Значение должно быть строкой",
                 "['dict_small'] Количество элементов не должно быть меньше длины 1",
                 "['dict_keys'][key1] Значение должно быть целым числом",
                 "['dict_big'] Количество элементов не должно быть больше длины 2"]
            )

        @validate.arguments(error_handler=test_exception_dict)
        def test_errors_dict(parent, data: validate.DictSchema(
            dict_big=validate.DictValue(
                max=2
            ),
            dict_small=validate.DictValue(
                min=1
            ),
            dict_keys=validate.DictValue(
                keys=validate.IntValue()
            ),
            dict_values=validate.DictValue(
                values=validate.StringValue()
            )
        )):
            self.fail(data)

        test_errors_dict(self, {
            'dict_big': {'key1': True, 'key2': False, 'key3': True},
            'dict_small': {},
            'dict_keys': {'key1': True},
            'dict_values': {'key1': True}
        })

        #
        # test date
        #

        def test_exception_date(a, info=False):
            self.assertCountEqual(
                info,
                ["['date_small'] Значение не должно быть меньше даты 2014-09-22 13:00:38.119173",
                 "['date_big'] Значение не должно быть больше даты 2014-09-22 13:00:38.119173"]
            )

        @validate.arguments(error_handler=test_exception_date)
        def test_errors_date(parent, data: validate.DictSchema(
            date_big=validate.DatetimeValue(
                max=datetime(2014, 9, 22, 13, 0, 38, 119173)
            ),
            date_small=validate.DatetimeValue(
                min=datetime(2014, 9, 22, 13, 0, 38, 119173)
            )
        )):
            self.fail(data)

        test_errors_date(self, {
            'date_big': datetime(2024, 9, 22, 13, 0, 38, 119173),
            'date_small': datetime(2000, 9, 22, 13, 0, 38, 119173)
        })

if __name__ == '__main__':
    unittest.main()
