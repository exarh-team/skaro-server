#!/usr/bin/env python
# Skaro is the open standard for instant messaging
# Copyright © 2017 Exarh Team
#
# This file is part of skaro-server.
#
# Skaro-server is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import cbor

# Import test module
# from server import parse_command_line, options, Application
from modules import server, database, config, helpers

from base64 import b64decode
from Crypto.Hash import SHA256
from Crypto.Cipher import PKCS1_v1_5
from Crypto.PublicKey import RSA

from tornado import testing, httpserver, gen, websocket, ioloop
from tornado.options import define, options, parse_command_line

import hmac
import hashlib
import logging
from tests._test_users import TestUsers
from tests._test_messages import TestMessages
from tests._test_conferences import TestConferences

logger = logging.getLogger(__name__) # named logging for test__main__.py

define("port", default=8000, type=int, help="run on the given port")


class TestServerHandler(TestUsers, TestMessages, TestConferences, testing.AsyncHTTPTestCase):

    ws_client = None
    resp_buffer = []
    session_id = None

    def setUp(self):
        super(TestServerHandler, self).setUp()

        # add configuration of server from command line(stored in options)
        # parse_command_line()

        if config.read("host") is None:
            print('Please set host in config')
            sys.exit(1)

        # Run server
        # app = Application()
        # server = httpserver.HTTPServer(app)
        # socket, self.port = testing.bind_unused_port()
        # server.add_socket(socket)

        # Run ws client
        ioloop.IOLoop.current().run_sync(self._mk_client)

    def get_app(self):
        return server.Application()

    @gen.coroutine
    def _mk_client(self):
        self.ws_client = yield websocket.websocket_connect(
            "ws://localhost:" + str(self.get_http_port()) + "/skaro_client/"
        )

    # Декодируем сообщение от клиента
    @gen.coroutine
    def read_server(self):
        data = yield self.ws_client.read_message()
        return cbor.loads(data)

    @gen.coroutine
    def read_request(self, req_condition):

        # Read from buffer
        for i, resp in enumerate(self.resp_buffer):
            if req_condition(resp):
                del self.resp_buffer[i]
                return resp

        # Read new message
        # todo: нужно ввести timeout на выполнение операции
        resp = yield self.read_server()

        if req_condition(resp):
            return resp
        else:
            self.resp_buffer.append(resp)

    # Отправляем сообщение клиенту
    def write_server(self, message):
        message = cbor.dumps(message)
        self.ws_client.write_message(message, binary=True)



    @testing.gen_test
    def test_1_users(self):
        # test register_type
        yield self._test_user_register_type_1()

        # test logout
        yield self._test_user_logout_1()

        # test register
        database.query("TRUNCATE users")
        yield gen.sleep(0.1)
        yield self._test_user_register_1()
        yield self._test_user_register_2()

        # test authorize
        yield self._test_user_authorize_1()
        self.ws_client.close()

        yield gen.sleep(0.1)
        self._mk_client()
        yield gen.sleep(0.1)

        # test session_restore
        yield self._test_user_session_restore_1()

        # test add_contact
        yield self._test_user_add_contact_1()

        # test add_contact
        yield self._test_user_add_contact_2()

        # test search_contacts
        yield self._test_user_search_contacts_1()

        # test update_my_data
        yield self._test_user_update_my_data_1()

        # test _test_user_get_contacts_1
        yield self._test_user_get_contacts_1()

        # test get_user_data
        yield self._test_user_get_user_data_1()

        # test del_contact
        yield self._test_user_del_contact_1()

        # test logout
        yield self._test_user_logout_2()

    @testing.gen_test
    def test_2_messages(self):
        yield self._test_user_authorize_2()

        # test send_message
        database.query("TRUNCATE history; ALTER SEQUENCE history_mid_seq RESTART WITH 1;")
        yield gen.sleep(0.1)
        yield self._test_message_send_message_1()

        # test get_history
        yield self._test_message_get_history_1()

        # test clear_history
        yield self._test_message_clear_history_1()

    @testing.gen_test
    def test_3_conferences(self):

        yield self._test_user_authorize_2()

        # test add_conference
        database.query("TRUNCATE conferences")
        yield gen.sleep(0.1)
        yield self._test_conference_add_conference_1()

        # test update_conference
        yield self._test_conference_update_conference_1()

        # test change_conference_members
        yield self._test_conference_change_conference_members_1()

if __name__ == '__main__':
    testing.main()
