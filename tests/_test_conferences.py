#!/usr/bin/env python
# Skaro is the open standard for instant messaging
# Copyright © 2017 Exarh Team
#
# This file is part of skaro-server.
#
# Skaro-server is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from tornado import testing, gen
import logging
import sys
from tornado.options import parse_command_line

from modules import helpers

logger = logging.getLogger(__name__)

# add configuration of server from command line(stored in options)
parse_command_line(helpers.parse_args(sys.argv))


class TestConferences:

    @gen.coroutine
    def _test_conference_add_conference_1(self):
        self.write_server({
            'type': "add_conference",
            'chain_uuid': "45",
            'login': "temp-conf",
            'name': "Тестирование конфы",
            'members': {
                '@test-another-user': {
                    'group': "member"
                }
            }
        })

        response = yield self.read_request(lambda x: x["type"] == "request" and x["chain_uuid"] == "45")
        self.assertEqual(response, {
            'type': 'request',
            'chain_uuid': "45",
            'status': '200 OK'
        })

    @gen.coroutine
    def _test_conference_update_conference_1(self):
        self.write_server({
            'type': "update_conference",
            'chain_uuid': "77",
            'login': "temp-conf",
            'update': {
                'name': "Моя конференция",
                'description': "Конференция (от лат. confero — собирать в одно место) — собрание, "
                               "совещание групп лиц, отдельных лиц, организации для обсуждения определённых тем."
            }
        })

        response = yield self.read_request(lambda x: x["type"] == "request" and x["chain_uuid"] == "77")
        self.assertEqual(response, {
            'chain_uuid': "77",
            'type': 'request',
            'status': '200 OK'
        })

    @gen.coroutine
    def _test_conference_change_conference_members_1(self):
        self.write_server({
            'type': "change_conference_members",
            'chain_uuid': "24",
            'login': "temp-conf",
            'del_members': {
                '@test-another-user': {
                    'reason': "плохой глупый бот"
                }
            }
        })

        response = yield self.read_request(lambda x: x["type"] == "request" and x["chain_uuid"] == "24")
        self.assertEqual(response, {
            'chain_uuid': "24",
            'type': 'request',
            'status': '200 OK'
        })


if __name__ == '__main__':
    parse_command_line()
    testing.main()
