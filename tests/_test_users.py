#!/usr/bin/env python
# Skaro is the open standard for instant messaging
# Copyright © 2017 Exarh Team
#
# This file is part of skaro-server.
#
# Skaro-server is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from tornado import testing, gen
import logging
import sys
from base64 import b64decode
from tornado.options import parse_command_line

from modules import helpers

from Crypto.Hash import SHA256
from Crypto.Cipher import PKCS1_v1_5
from Crypto.PublicKey import RSA

logger = logging.getLogger(__name__)

# add configuration of server from command line(stored in options)
parse_command_line(helpers.parse_args(sys.argv))


class TestUsers:

    def __init__(self, *args, **kwargs):
        super(TestUsers, self).__init__(*args, **kwargs)

        self.public_key = """-----BEGIN PUBLIC KEY-----
MIGeMA0GCSqGSIb3DQEBAQUAA4GMADCBiAKBgHbvp+hxXbDhZjAWvmmQPRBW7dja
FGinYVX8yeUiNTfcEvgu8zMEU1pzn5KAjLkRMFIGPEnV6Ne4Q8gnrQjUxu94dOsY
TKqj3QsYLE0S2VS913IIyOYQyDnoudQRobFrXDVe4yrDRxAXKOlT8weXOJm5hgwn
1PxTJsBq/u5bPQs1AgMBAAE=
-----END PUBLIC KEY-----"""

        self.private_key = """-----BEGIN RSA PRIVATE KEY-----
MIICWwIBAAKBgHbvp+hxXbDhZjAWvmmQPRBW7djaFGinYVX8yeUiNTfcEvgu8zME
U1pzn5KAjLkRMFIGPEnV6Ne4Q8gnrQjUxu94dOsYTKqj3QsYLE0S2VS913IIyOYQ
yDnoudQRobFrXDVe4yrDRxAXKOlT8weXOJm5hgwn1PxTJsBq/u5bPQs1AgMBAAEC
gYAbp9hsRUga5B6F2hQGqeJ5FS2TOgp610jb3L75h73GMmc6zHgCiePz6WTWhQuz
0NpiiE6mgJX260LfnNIwdoGXRgm+ye4TT+y7VPi9Ql+MngKHGssfYBwN+9QJvDPG
znkXe9F/pNn6UlkXp7LNad/KnjeWKMB1tJ9tGyoXCzNL9QJBAL058+iGw31b/cL+
ShiVWhAIVWBEdlMC/sdRiY7b5jt0/uYvbm277Jt7H8EBHaFDLypE8kpWTF5h/260
5pBg0gsCQQCg5+5WyFn9BtF3UGmAAXBDN4hZ7YLr/nr8R/5w34NIf6XMzFak11YC
UN8/jAr563aFNH7WnRpGj4oGquxwUx+/AkBVvqoApvh0mLe/oTzwMNUzyKLvUXaQ
nR3YMeF0Y77LGHPc2IdgoDRSLBOygI/toCUCMWXdO4e1iRCXGNAVgloLAkA8TzIy
LN4tzIRuaCFi7ScaypG8wx0zPyrxrDpeLCiU3+NRI7XGC0AdlhpXQzJKAktmGgsA
fdn/YIWngiEk6MMZAkEAqYRynony1okEs/QM83GyJJ5UFbvbRMbY53ds1lI2yxzR
I5itCYSUGwlE19lQeUJXerNCSDiZ8pAC9d7OUqontQ==
-----END RSA PRIVATE KEY-----"""

    @gen.coroutine
    def _test_user_register_type_1(self):
        self.write_server({
            'chain_uuid': "3",
            'type': "register_type"
        })
        response = yield self.read_request(lambda x: x["type"] == "request" and x["chain_uuid"] == "3")
        self.assertEqual(response, {
            'chain_uuid': "3",
            'type': 'request',
            'status': '200 OK',
            'register_type': 'open'
        })

    @gen.coroutine
    def _test_user_logout_1(self):
        self.write_server({
            'type': "logout",
            'chain_uuid': "1"
        })
        response = yield self.read_request(lambda x: x["type"] == "request" and x["chain_uuid"] == "1")
        self.assertEqual(response, {
            'chain_uuid': "1",
            'type': 'request',
            'info': 'Данный тип сообщений поддерживается только после авторизации или не поддерживается вообще.',
            'status': '401 Unauthorized'
        })

    @gen.coroutine
    def _test_user_register_1(self):
        self.write_server({
            'type': "register",
            'chain_uuid': "7",
            'login': "test-user",
            'email': "test@host.lh",
            'public_key': self.public_key
        })

        response = yield self.read_request(lambda x: x["type"] == "request" and x["chain_uuid"] == "7")
        self.assertEqual(response, {
            'chain_uuid': "7",
            'type': 'request',
            'status': '200 OK',
            'login': 'test-user'
        })

    @gen.coroutine
    def _test_user_register_2(self):
        self.write_server({
            'type': "register",
            'chain_uuid': "27",
            'login': "test-another-user",
            'email': "test@localhost.lh",
            'public_key': self.public_key
        })

        response = yield self.read_request(lambda x: x["type"] == "request" and x["chain_uuid"] == "27")
        self.assertEqual(response, {
            'chain_uuid': "27",
            'type': 'request',
            'status': '200 OK',
            'login': 'test-another-user'
        })

    @gen.coroutine
    def _test_user_authorize_1(self):
        self.write_server({
            'type': "authorize",
            'chain_uuid': "3",
            'login': "test-user"
        })
        response = yield self.read_request(lambda x: x["type"] == "request" and x["chain_uuid"] == "3")

        encrypted_rnd = response['encrypted_rnd']
        self.assertEqual(len(response['encrypted_rnd']), 172)

        del response['encrypted_rnd']

        self.assertEqual(response, {
            'chain_uuid': "3",
            'type': 'request',
            'status': '200 OK'
        })

        decrypter = PKCS1_v1_5.new(RSA.importKey(self.private_key))
        rnd = decrypter.decrypt(b64decode(encrypted_rnd), '')
        self.assertEqual(type(rnd), bytes)
        rnd_hash = SHA256.new(rnd).hexdigest()

        self.write_server({
            'type': "request",
            'chain_uuid': "3",
            'rnd_hash': rnd_hash,
            'status': "200 OK"
        })
        response = yield self.read_request(lambda x: x["type"] == "request" and x["chain_uuid"] == "3")
        self.assertEqual(len(response['session_id']), 36)
        type(self).session_id = response['session_id']
        del response['session_id']
        del response['device_id']
        del response['user']['login']
        self.assertEqual(response, {
            'chain_uuid': "3",
            'type': 'request',
            'status': '200 OK',
            'user': {
                'contacts': {},
                'name': '',
                'status': 'online',
                'accounts': {},
                'current_info': '',
                'email': 'test@host.lh'
            }
        })

    @gen.coroutine
    def _test_user_authorize_2(self):
        self.write_server({
            'type': "authorize",
            'chain_uuid': "34",
            'login': "mylogin"
        })
        response = yield self.read_request(lambda x: x["type"] == "request" and x["chain_uuid"] == "34")

        chain_uuid = response['chain_uuid']
        encrypted_rnd = response['encrypted_rnd']
        self.assertEqual(len(response['encrypted_rnd']), 172)

        del response['encrypted_rnd']

        self.assertEqual(response, {
            'chain_uuid': "34",
            'type': 'request',
            'status': '200 OK'
        })

        decrypter = PKCS1_v1_5.new(RSA.importKey(self.private_key))
        rnd = decrypter.decrypt(b64decode(encrypted_rnd), '')
        self.assertEqual(type(rnd), bytes)
        rnd_hash = SHA256.new(rnd).hexdigest()

        self.write_server({
            'type': "request",
            'chain_uuid': "34",
            'rnd_hash': rnd_hash,
            'status': "200 OK"
        })
        response = yield self.read_request(lambda x: x["type"] == "request" and x["chain_uuid"] == "34")
        self.assertEqual(len(response['session_id']), 36)
        type(self).session_id = response['session_id']
        del response['session_id']
        del response['device_id']
        del response['user']['login']
        del response['user']['contacts']
        self.assertEqual(response, {
            'chain_uuid': "34",
            'type': 'request',
            'status': '200 OK',
            'user': {
                'name': '',
                'status': 'online',
                'accounts': {},
                'current_info': 'Какая-то важная инфа',
                'email': 'test@host.lh'
            }
        })

    @gen.coroutine
    def _test_user_session_restore_1(self):
        self.write_server({
            'type': "session_restore",
            'chain_uuid': "34",
            'session_id': type(self).session_id
        })
        response = yield self.read_request(lambda x: x["type"] == "request" and x["chain_uuid"] == "34")
        self.assertEqual(len(response['session_id']), 36)
        type(self).session_id = response['session_id']
        del response['session_id']
        del response['user']['login']
        self.assertEqual(response, {
            'chain_uuid': "34",
            'type': 'request',
            'status': '200 OK',
            'user': {
                'status': 'online',
                'name': '',
                'contacts': {},
                'accounts': {},
                'email': 'test@host.lh',
                'current_info': ''
            }
        })

    @gen.coroutine
    def _test_user_add_contact_1(self):
        self.write_server({
            'type': "add_contact",
            'chain_uuid': "12",
            'login': "test-another-user"
        })

        response = yield self.read_request(lambda x: x["type"] == "request" and x["chain_uuid"] == "12")
        del response['user']['login']
        self.assertEqual(response, {
            'chain_uuid': "12",
            'type': 'request',
            'user': {
                'contacts': {},
                'status': 'offline',
                'accounts': {},
                'email': 'test@localhost.lh',
                'name': '',
                'current_info': ''
            },
            'status': '200 OK'
        })

    @gen.coroutine
    def _test_user_add_contact_2(self):
        self.write_server({
            'type': "add_contact",
            'chain_uuid': "55",
            'login': "test-another-user"
        })

        response = yield self.read_request(lambda x: x["type"] == "request" and x["chain_uuid"] == "55")
        self.assertEqual(response, {
            'chain_uuid': "55",
            'type': 'request',
            'info': 'Этот пользователь или конференция уже является вашим контактом',
            'status': '400 Bad Request'
        })

    @gen.coroutine
    def _test_user_search_contacts_1(self):
        self.write_server({
            'type': "search_contacts",
            'chain_uuid': "4355",
            'query': "test-"
        })

        response = yield self.read_request(lambda x: x["type"] == "request" and x["chain_uuid"] == "4355")
        del response['result'][0]['login']
        self.assertEqual(response, {
            'result': [
                {
                    'current_info': '',
                    'name': '',
                    'match_val': 0.833333333333333
                }
            ],
            'chain_uuid': "4355",
            'status': '200 OK',
            'type': 'request'
        })

    @gen.coroutine
    def _test_user_update_my_data_1(self):
        self.write_server({
            'type': "update_my_data",
            'chain_uuid': "3654",
            'update': {
                'login': "mylogin",
                'current_info': "Какая-то важная инфа"
            }
        })

        response = yield self.read_request(lambda x: x["type"] == "request" and x["chain_uuid"] == "3654")
        self.assertEqual(response, {
            'chain_uuid': "3654",
            'type': 'request',
            'status': '200 OK'
        })

    @gen.coroutine
    def _test_user_get_contacts_1(self):
        self.write_server({
            'type': "get_contacts",
            'chain_uuid': "99"
        })

        response = yield self.read_request(lambda x: x["type"] == "request" and x["chain_uuid"] == "99")
        del response['data'][0]['login']
        self.assertEqual(response, {
            'chain_uuid': "99",
            'type': 'request',
            'status': '200 OK',
            'data': [
                {
                    'current_info': '',
                    'status': 'offline',
                    'name': '',
                    'contacts': {},
                    'email': 'test@localhost.lh',
                    'accounts': {}
                }
            ]
        })

    @gen.coroutine
    def _test_user_get_user_data_1(self):
        self.write_server({
            'type': "get_user_data",
            'chain_uuid': "54",
            'login': "test-another-user"
        })

        response = yield self.read_request(lambda x: x["type"] == "request" and x["chain_uuid"] == "54")
        del response['data']['login']
        self.assertEqual(response, {
            'chain_uuid': "54",
            'type': 'request',
            'data': {
                'current_info': '',
                'contacts': {},
                'name': '',
                'status': 'offline',
                'email': 'test@localhost.lh',
                'accounts': {}
            },
            'status': '200 OK'
        })

    @gen.coroutine
    def _test_user_del_contact_1(self):
        self.write_server({
            'type': "del_contact",
            'chain_uuid': "54",
            'login': "test-another-user"
        })

        response = yield self.read_request(lambda x: x["type"] == "request" and x["chain_uuid"] == "54")
        self.assertEqual(response, {
            'chain_uuid': "54",
            'type': 'request',
            'status': '200 OK'
        })

    @gen.coroutine
    def _test_user_logout_2(self):
        self.write_server({
            'type': "logout",
            'chain_uuid': "1"
        })


if __name__ == '__main__':
    parse_command_line()
    testing.main()
