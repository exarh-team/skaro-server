#!/usr/bin/env python
# Skaro is the open standard for instant messaging
# Copyright © 2017 Exarh Team
#
# This file is part of skaro-server.
#
# Skaro-server is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys

# Tornado server
import tornado.ioloop
from tornado.options import define, options, parse_command_line



# Local modules and packages
from modules import config, server

define("host", default=config.read("host"), type=str,
       help="domain of the site running the server (without http:// or https://)")
define("port", default=8000, type=int, help="run on the given port")


if __name__ == "__main__":
    # add configuration of server from command line(stored in options)
    parse_command_line()

    if options.host is None:
        print('Please set host in config or use --host=')
        sys.exit(1)

    # Run server
    app = server.Application()
    app.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()
